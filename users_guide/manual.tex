\documentclass[final,times,onecolumn]{article}
\usepackage{amsmath}


\title{The DEPOSIT 2014 computer code. Manual.}


\author{Mikhail S. Litsarev}



\begin{document}

\maketitle

\section{Installation}

The code installation requires to compile the sources only.
It is needed C++ and Fortran compilers and BLAS and LAPACK
external libraries. For spline interpolation the ALGLIB library
is used (http://www.alglib.net). Sources of this library are included in the
distribution.

To compile the code one should point out the C++ and FORTRAN compilers
in the makefile and give a \texttt{make} command in the source code directory.
By default it is used GNU C++ and GNU Fortran compilers.
\begin{verbatim}
CompCC = g++ -O2
CompFF = gfortran -O2
\end{verbatim}
We suppose that in your system it is used not
ancient version of the C++ compiler, otherwise
you probably need to modify the names
of the included headers in the source files by the following way:
\texttt{\#include <ctime>} to \texttt{\#include <time.h>}.

\noindent
To compile the code you need link LAPACK, BLAS and FORTRAN libraries.
On Mac Os X it may seems like
\begin{verbatim}
main$(x): $(code)
    $(CompCC) $(code) -llapack -lblas -lgfortran -L/usr/local/Cellar/
gfortran/4.8.1/gfortran/lib 
\end{verbatim}

On Linux you probably need to specify include path
by adding the option \texttt{-I/usr/local/...[path to LAPACK]/include/}
or something like that.

In our code we use LAPACK routines with underscore at the end:
the FORTRAN routine \texttt{DGEQRF} is used as \texttt{dgeqrf\_} in the C++ code.



\section{Input/output}
\label{ExamplesIO3}

\subsection{Input}
\label{SubSecInp}
All input parameters for the program  are set up by means of
one input text file with the proper keywords.
Every keyword can be put in arbitrary place
of the input file without
a definite compliance of the order.
The program is case-sensitive to the keywords.
Also there can be put in comments in
$C$/$C$++ programming language style.

The keywords are: \texttt{Vi} - velocity
of the projectile in frame of the
neutral atomic target given in atomic units,
 \texttt{Za} - atomic charge and
\texttt{Ra} - effective radius of the atomic target.
Each of these parameters should be followed by
one or several spaces and a real number (the value of the parameter).
For example, for the $Ba^{2+}+O$ collision
at the energy $E=2.5$~MeV/u they are
\begin{verbatim}
 Vi     10.0
 Za      8.0
 Ra      1.239652
\end{verbatim}
Two values $A_1$ and $A_2$
are given after the keyword \texttt{A\_exp}.
Three values $\alpha_1$, $\alpha_2$, $\alpha_3$
for the equation are given after
the keyword \texttt{alf\_exp}.
\begin{verbatim}
A_exp     0.0625   0.9375
alf_exp   14.823   2.0403   0.0
\end{verbatim}
These parameters can be found in the ref~\cite{DHFS_fitt}.

First integer value after the keyword \texttt{shells}
sets up the number of the ion's $\gamma$-shells.
The number of electrons
$N_{\gamma}$, the values of the coefficients
$\mu_{\gamma}$ and $\beta_{\gamma}$
for the Slater type density and binding energy $I_{\gamma}$
must be put in after as well.
In the new version 2014 the column for $C_{1\gamma}$ is
excluded. These coefficients are calculated automatically in the program.

For every $\gamma$-shell it is more convenient
to set up these four parameters from a new line.
Rules for calculation of  $\mu_{\gamma}$ and $\beta_{\gamma}$
parameters are given in
the references~\cite{nimb09},~\cite{ShevSlaterPrm2001}.
The binding energies $I_{\gamma}$ are put in
electron-volts~(eV). The values of $I_{\gamma}$ can be
found in \cite{Desclaux}-\cite{IbRef4}.
\begin{verbatim}
 Shells 7
//N    mu    beta     I(eV)
  8   4.0   2.5625    33.11   // 5sp8
 10   3.5   4.8143   106.044  // 4d10
  8   3.5   8.0714   215.52   // 4sp8
 10   3.0  11.6167   801.35   // 3d10
  8   3.0  14.9167  1152.6    // 3sp8
  8   2.0  25.925   5542.13   // 2sp8
  2   1.0  55.7    37455.41   // 1s2
\end{verbatim}

The deposited energy $T(b)$ and the total cross section $\sigma_{tot}$
are calculated automatically. There is no needed any keyword for this.

To calculate $m$-fold cross sections
$\sigma_m$, where $1\le m \le N'$, one needs
to give a keyword \texttt{Sigma\_m\_fold} with
$N'+1$ values:
the value of $N'$ parameter and
$N'$ values of $m$-th ionization
potentials $I_m$~[eV]
\begin{verbatim}
// m-fold cross section
   Sigma_m_fold  30
   34.45   48.40   62.35   76.30   92.53
   107.1   139.2   155.7   232.2   266.0
   299.8   333.6   367.4   401.2   437.8
   472.0   506.2   540.5   672.6   710.3
   747.9   785.6   839.2   878.2   975.9
  1016.0  1586.0  1688.0  1790.0  1892.0
\end{verbatim}

\noindent
The remaining parameters have technical sense.
\begin{verbatim}
 ksmear     3.0
 x_space    12  2000 
 y_space    12  2000 
 cross2d    1e-5             
 maxvol     1e-2             
 gauss_cut  1e-20     
 tk_mesh    400 -3.0 45.0  
\end{verbatim}
Parameter \texttt{ksmear} fixes the $k$ smearing value in the Fermi-smearing
step (see equation (14) in ref~\cite{cpc2013}).
Parameter \texttt{x\_space} sets up 
x-integration bounds \phantom{x} \texttt[-12,12] and number of points $N_x$.
See equations (23), (25) and (26) of ref.~\cite{arxiv2014}.
Parameter \texttt{y\_space} does the same for y-integration,
equations (24), (27) and (28). We note integration
on second variable by dummy $y$, although it is $\bar z$
in a strict formal sense.
Parameter \texttt{cross2d} sets up the accuracy of the integrand 
$\Delta E(x,z - b)$ approximation in the deposited energy integral $T(b)$,
see equations (7) and (20) ref.~\cite{arxiv2014}. This accuracy controls the calculation of $T(b)$ and is important.
The \texttt{maxvol} accuracy is a technical parameter and can
be kept equal to \texttt{1e-2}. The \texttt{gauss\_cut}
truncation holds $w_k$ not less then \texttt{1e-20} in 
equation~(B.3) ref.~\cite{arxiv2014}.
Parameter \texttt{tk\_mesh} sets values for $K$, $a_t$ and $b_t$,
equation~(B.4), ref.~\cite{arxiv2014}.



\subsection{Output}
\label{SubSecOut}
The name of correctly composed input file
is passed to the program input
with the name of the output file
via command line when program starts.
The output of the program is sent to console.
\begin{verbatim}
> ./a.out file_name1_inp file_name2_out
\end{verbatim}

The program writes down a text file
called \texttt{Tb\_[file\_name2\_out]} with the deposited energy,
The file consists of several columns.
First column contains the $b$-values,
second column is the $T(b)$ energy
and the rest columns are the deposited energies
for every $\gamma$-shell.

The program writes down a text file
called \texttt{Pm\_[file\_name2\_out]}
with the probabilities $P_m(b)$,
if the \texttt{Sigma\_m\_fold} keyword is given.
The file consists of several columns.
First column is the $b$-values
and the other ones are values of the $P_m(b)$
probabilities for the corresponding $b$ points
and $1 \le m \le N'$.

All input parameters after they have been checked
are printed out to console
(for user's control) and then program starts the calculations.
There is only single version of the program.
No parallel version is released due to the fast program run.

\begin{verbatim}
 ****  CODE DEPOSIT 2014 (VERSION 2.0/S) ****  

 Input reading from file:  ba2+_o.txt

 Vi = 10.0  Za = 8  Ra = 1.23965

 A_exp:     0.06250   0.93750   0.00000
 alf_exp:  14.82300   2.04030   0.00000

 Shells = 7
   N        C1     mu   beta      I[eV]        I[au]     u     Neff
  8.0      7.778  4.0   2.562      33.11       1.22    1.56     ---
 10.0    121.070  3.5   4.814     106.04       3.90    2.79     ---
  8.0    956.534  3.5   8.071     215.52       7.92    3.98     ---
 10.0   2252.830  3.0  11.617     801.35      29.45    7.67     ---
  8.0   5404.971  3.0  14.917    1152.60      42.36    9.20     ---
  8.0   3951.539  2.0  25.925    5542.13     203.67   20.18   0.0511
  2.0    831.405  1.0  55.700   37455.41    1376.46   52.47   0.0019
\end{verbatim}
The $N_{\mbox{\footnotesize{eff}}}^{(\gamma)}$ parameter
is used only if $v \le u_{\gamma}$.
In this case the $N_{\mbox{\footnotesize{eff}}}^{(\gamma)}$
values are printed out in the \texttt{Neff} column.
Otherwise, when $v > u_{\gamma}$, the dashed line
is printed only.
\begin{verbatim}
 ksmear = 3.0000
 Maxvol tolerance = 0.01
 Cross2D truncation error = 1e-05
 Gauss truncation error = 1e-20
 tk_mesh: 400  [-3.00, 45.00]
\end{verbatim}
According to equations (23), (24), (29)--(33) of ref.~\cite{arxiv2014}
it prints out the rect of 2D integration and number of
homogeniously distributed points.
\begin{verbatim}
 Function Generator: 
 Domain is a [-12.00,12.00] x [-24.00,12.00] rectangle
 with n_x = 4001, m_y = 6001 points.
\end{verbatim}

The deposited energy $T(b)$ is calculated for every shell
according to the cross algorithm, see ref.~\cite{arxiv2014}.
\begin{verbatim}
  Start calculation of Tb procedure:

  Shell(0):   8.0       7.778   4.0    2.562
   Rho size: 117 / 401 = 0.292;   Contraction: (0.228, 0.228)
   Rho error:  integral  = 5.549e-17
               local max = 8.309e-17  in point r0 = 0.006
   Cross time = 0.329 Sec.    Rank = 10
   Low rank integration (2001 integrals): 100%
   Total time = 40.490 Sec.   Per integral = 2.024e-02 Sec.

 Shell(1):  10.0     121.070   3.5    4.814
   Rho size: 130 / 401 = 0.324;   Contraction: (0.114, 0.114)
   Rho error:  integral  = 2.111e-18
               local max = 4.665e-17  in point r0 = 0.000
   Cross time = 0.270 Sec.    Rank = 10
   Low rank integration (2001 integrals): 100%
   Total time = 22.168 Sec.   Per integral = 1.108e-02 Sec.
  . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

 Shell(6):   2.0     831.405   1.0   55.700
   Rho size: 340 / 401 = 0.848;   Contraction: (0.004, 0.004)
   Rho error:  integral  = 4.757e-10
               local max = 1.064e-08  in point r0 = 0.000
   Cross time = 0.369 Sec.    Rank = 14
   Low rank integration (2001 integrals): 100%
   Total time = 2.694 Sec.   Per integral = 1.346e-03 Sec.

 Output T(b) into the file:  Tb_ba_out.txt
\end{verbatim}
It prints out shell data ($N_{el}$, $C_1$, $\mu$, $\beta$).
\texttt{Rho size} means number of terms in equation (12) ref.~\cite{arxiv2014}
after truncation.
For every term in sum (12) ref.~\cite{arxiv2014} and
every point $i$ of $x$ or $\bar z$ mesh (see (26) and (30) ref.~\cite{arxiv2014})
the value of $\lambda_k e^{-\eta_k x_i^2}$ or $\lambda_k e^{-\eta_k \bar z_i^2}$
can be less then \texttt{gauss\_cut} value.
\texttt{Contraction} means the relation of such residual terms 
(greater then \texttt{gauss\_cut}) to the
total number of these terms for $x_i$ and $\bar z_j$  meshes.
\texttt{Rho error} prints the integral error 
\begin{equation}
\left( \int_{0}^{\infty} (\rho_s(r) - \rho_g(r))^2  dr \right)^{1/2}
\end{equation}
and the local error
\begin{equation}
\max_{i} \left| \rho_s(r_i) - \rho_g(r_i)  \right|
\end{equation}
for Gaussian approximation $\rho_g(r)=\sum_k \lambda_k e^{-\eta_k r^2}$
in comparision with Slater type electron density
$\rho_s(r)=NC_1^2 r^{2\mu} e^{-2\beta r}$.

\noindent
Then it prints out the time which the cross decomposition takes,
and the rank of the decomposition.
It also prints out the time needed for calculation of
all $T(b)$ integrals in all points of $b$-mesh and 
average time per integral.

For the $\sigma_{tot}$ calculations
program prints out the input parameters,
bisection search history, and interpolation data.
First it does discrete bisection on the calculated $T(b_l)$ array.
\begin{verbatim}
Total cross-section calculation:
 I1 =     1.2660 a.u.
 b_min =  0.0000   b_max = 12.0000

 Discrete bisection search:
      b       T(b) - I1
  0.000000    696.779529
 12.000000     -1.266015
  6.000000     -1.266004
  3.000000     -1.063813
  1.500000      5.993544
  2.250000      0.212222
  2.628000     -0.694736
  2.442000     -0.340900
  2.346000     -0.092182
  2.298000      0.052508
  2.322000     -0.021644
  2.310000      0.014972
  2.316000     -0.003450
\end{verbatim}
Then it does spline interpolation for 10 points region,
closest to the solution $T(b_0)-I_1=0$
and does continuous bisection search.
Finally, it prints out the $b_{\max}$
and $\sigma_{tot}$ values.
\begin{verbatim}
 Spline bisection search:
      b       T(b) - I1
  2.313000      0.005732
  2.314500      0.001134
  2.315250     -0.001160

 Interpolate:  
      b       T(b) - I1
  2.314687      0.000560
  2.314875     -0.000014
  2.315062     -0.000587

 b_total     =    2.314871
 Sigma_total =   16.834620 a.u.
 Sigma_total =    4.714172e-16 cm2  
\end{verbatim}

For the $m$-fold cross-section $\sigma_{m}$ calculations,
it prints out input values of ionization
potentials.
To this moment the $T(b)$ energy is calculated and the
$\sigma_{m}$ data are evaluated and printed out in a moment.
To check out the calculation the sum rule is applied
to the $\sigma_{m}$ values and printed after
the partial cross sections.
\begin{verbatim}
 m-fold cross-sections calculation
  I_1 =     1.2660 a.u. =       34.450 eV
  I_2 =     1.7787 a.u. =       48.400 eV
  . . . . . . . . . . . . . . . . . . . .
 I_31 =    73.2414 a.u. =     1993.000 eV
 I_32 =    76.9898 a.u. =     2095.000 eV
 I_33 =    81.3263 a.u. =     2213.000 eV

  m-fold Cross-sectio  m-fold Cross-sections:
    m       a.u.        cm2
    1    6.502628   1.820920e-16
    2    3.676642   1.029564e-16
    3    1.939753   5.431860e-17
    4    1.140665   3.194187e-17
    5    0.747169   2.092287e-17

   24    0.005909   1.654597e-19
   25    0.000595   1.665363e-20
   26    0.000019   5.355331e-22
   27    0.000000   1.347772e-25
   28    0.000000   3.248149e-31
   29    0.000000   0.000000e+00
   30    0.000000   0.000000e+00
   31    0.000000   0.000000e+00
   32    0.000000   0.000000e+00
   33    0.000000   0.000000e+00
  sum   16.834620   4.714172e-16

 Output Pm(b) into the file:  Pm_ba_out.txt

 Start time: Wed Jun  4 17:01:58 2014
 Stop  time: Wed Jun  4 17:03:42 2014
\end{verbatim}



\section{Structure of the code}

The DEPOSIT 2014 code consists of the following source files:

\noindent
\texttt{add\_lapack.h, 
ccross\_2d.h, ccross\_2d.cpp,
deposit\_parser.h}, 

\noindent
\texttt{deposit\_parser.cpp,
deposit\_solver\_lr.h, deposit\_solver\_lr.cpp},

\noindent
\texttt{energy\_gain.h, energy\_gain.cpp, 
fmaxvol.f90, 
integral\_tensor\_sum.h, integral\_tensor\_sum.cpp,	
main.cpp, makefile},

\noindent
\texttt{quad\_weights.h, quad\_weights.cpp,
probability\_m.h, probability\_m.cpp,
read.h, read.cpp,
rho\_gauss.h, rho\_gauss.cpp}.

We also use the following files from
the ALGLIB (www.alglib.net) project:
\texttt{alglibinternal.h, alglibinternal.cpp,
alglibmisc.h, alglibmisc.cpp,
ap.h, ap.cpp,
integration.h, integration.cpp},

\noindent
\texttt{interpolation.h, interpolation.cpp,
linalg.h, linalg.cpp,
solvers.h, solvers.cpp,
specialfunctions.h, specialfunctions.cpp
optimization.h, optimization.cpp,
stdafx.h}.

These library is used for special functions and
spline interpolation.

The auxiliary functions for input data reading are declared
in the \texttt{read.h} and defined in the \texttt{read.cpp} files.
These functions allows to read integer, real, and string 
data types from the input text file.

Class \texttt{DepositParser} parses the input file and contains
all input data to be used later in the program.

The \texttt{maxvol} algorithm~\cite{maxvol} is implemented in the \texttt{fmaxvol.f90}
module. It is needed for two dimensional cross approximation algorithm.
The \texttt{cross2D} algorithm~\cite{cross2d} is implemented in the 
\texttt{ccross\_2d.h}, \texttt{ccross\_2d.cpp} files.
File \texttt{add\_lapack.h} declares all BLAS/LAPACK routines
used in C++ code in the \texttt{cross2D} algorithm.


Slater density expansion via Gaussians is done in 
\texttt{rho\_gauss.h}, \texttt{rho\_gauss.cpp}.

Calculation of the energy gain $\Delta E$ 
on a homogenious mesh is implemented in 
files \texttt{energy\_gain.h} and \texttt{energy\_gain.cpp}

Files \texttt{quad\_weights.h} and \texttt{quad\_weights.cpp}
contain classes corresponding to the
various quadrature rules. They return coefficients for  trapezoid and Simpson
quadratures.

Main 3D-integration is implemented in the
\texttt{integral\_tensor\_sum.h} and \texttt{integral\_tensor\_sum.cpp}.
For every shell the class \texttt{IntegralTb\_TensorSum} does low rank integration
according to the equations (25) and (33) of ref.~\cite{arxiv2014}.


In the \texttt{probability\_m.h/cpp}
files the \texttt{Probability\_mfold}
 class is defined. It does calculations 
of probabilities, defined in the Russek-Meli Model.
Calculations are done very 
carefully using expansions of the limiting
cases in zero and infinity values of $m$.
The power overflow problem is also avoided
by using of the exponential values of logarithms.
Input data are read by the function
\texttt{bool read (const char file[])}.
Values of probabilities can be written by 
\texttt{void write (const char file[]) const}
function. The function
\texttt{void set\_b\_Tb\_array(...)}
get an array of $T(b)$ values in every $b$ point
$0 \le b \le b_{\max}$ and then use it
to do $P_m(b)$ calculations.

Finally, the main Deposit solver is declared in the \texttt{deposit\_solver\_lr.h}
file and is defined in the \texttt{deposit\_solver\_lr.cpp} file
as DepositSolverLR class. In the 
\texttt{void solve(const char inp\_file[], const char out\_file[])}
function it  calculates the $T(b)$ dependency using the function
\texttt{void calc\_energy\_Tb\_()}, calculates total cross-section  using the function
\texttt{void calc\_Sigma\_tot\_()}, and
calculates $m$-fold cross sections, using the function
\texttt{void calc\_Sigma\_m\_fold\_()}.







\begin{thebibliography}{99}

\bibitem{DHFS_fitt}
F. Salvat, J.D. Martinez, R. Mayol, J. Parellada,
Phys. Rev. A \textbf{36} (1987) 467.

\bibitem{nimb09}
M.-Y. Song, M.S. Litsarev, V.P. Shevelko, H. Tawara, J.-S. Yoon,
Nucl. Instr. Meth. in Phys. Research B \textbf{267} (2009) 2369.
\bibitem{ShevSlaterPrm2001}
V.P. Shevelko, L.A. Vainshtain,
Atomic physics for hot plasmas, Bristol:IOP (1993).

\bibitem{Desclaux}
J.P. Desclaux, At. Data Nucl. Data Tables \textbf{12} (1973) 311.

\bibitem{IbRef2}
T.A. Carlson, C.W. Nestor Jr., N. Wasserman, J.P. McDowell,
Atom. Data Nucl. Data Tables \textbf{2} (1970) 63.
\bibitem{IbRef3}
K. Rashid, M.Z. Saadi, M. Yasin,
At. Data Nucl. Data Tables \textbf{40} (1988) 365.
\bibitem{eBindBa2}
W. Lotz, JOSA \textbf{60} (1970) 206.
\bibitem{IbRef4}
G. Zschornack, G. Musiol, W. Wagner,
Dirack-Fock-Slater X-ray energy shifts and electron binding
energy changes for all ion ground states in elements up to uranium.
Preprint ZfK-574 Zentralinstitut fur Kernforschung,
Rossendorf bei Dresden (1986).

\bibitem{cpc2013}
M.S. Litsarev, Comput. Phys. Comm. \textbf{184} (2013) 432.

\bibitem{arxiv2014}
M.S. Litsarev, I.V. Oseledets, arXiv:1403.4068.

\bibitem{maxvol}
S. Goreinov, I.V. Oseledets, D.V. Savostyanov, E.E. Tyrtyshnikov 
\textit{at al}
Matrix methods: theory, algorithms and applications, 247 (2010).


\bibitem{cross2d}
E.E. Tyrtyshnikov. Incomplete cross approximation in the mosaic-skeleton 
 method. Computing, \textbf{64} (2000) 367.



\end{thebibliography}


\end{document}


% End of the file

