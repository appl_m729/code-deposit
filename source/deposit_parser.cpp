#include <iostream>
#include <iomanip>
#include <cmath>
#include "deposit_parser.h"
#include "read.h"
#include "specialfunctions.h"

using std::cout;
using std::endl;
using std::setprecision;
using std::setw;

///////////////////////////////////////////////////////////////////////////////

DepositParser::DepositParser():
Vi_(0), Za_(0), Ra_(0),
Ebohr_(13.60569253),  // Check it
N_shells_(0), Nel_tot_(0), Nel_(0),
C1_(0), C2_(0), C3_(0),
I_(0), Zeff_(0), u_(0), ksmear_(3.0),  
I1_first_(0),
maxvol_tolerance_(1e-2),
ax_left_bound_(-10.0),
bx_right_bound_(10.0),
ay_left_bound_(-10.0),
by_right_bound_(10.0),
Nx_(1024), Ny_(1024),
cross_error_(1e-6),
gauss_cutoff_(1e-20) {
}

DepositParser::~DepositParser() {

    delete[] Nel_;
    delete[] C1_;
    delete[] C2_;
    delete[] C3_;
    delete[] I_;
    delete[] Zeff_;
    delete[] u_;
}

void DepositParser::setup(const char file[]) {

    cout << " Input reading from file:  " << file << endl << endl;

    // read V ion
    int key = ReadReal(file,"Vi", 0, Vi_);
    if (key != 0) { cout << "Error read v" << endl; exit(0);}
    cout << " Vi = " << Vi_ << " ";

    // read Z atom
    key = ReadReal(file,"Za", 0, Za_);
    if (key != 0) { cout << "Error read Za" << endl; exit(0);}
    cout << " Za = " << Za_ << " ";

    // read R atom
    key = ReadReal(file,"Ra", 0, Ra_);
    if (key != 0) { cout << "Error read Ra" << endl; exit(0);}
    cout << " Ra = " << Ra_ << " " << endl << endl;

//------------------A1--A2--A3--alf1--alf2--alf3--parameters------------------
    double sum_A(0);
    for (int i = 0; i < N3; ++i) {

        // read alpha_i exp
        key = ReadReal(file,"alf_exp", i, alpha_exp_[i]);
        if (key != 0){ cout << "Error read alpha_exp" << endl; exit(0);}

        // read A_i exp
        // A1 + A2 + A3 = 1
        if (i < (N3 - 1)) {

            key = ReadReal(file,"A_exp", i, A_exp_[i]);
            if (key != 0) {cout << "Error read A_exp" << endl; exit(0);}

        } else if ((N3 - 1) == i) {
            A_exp_[2] = 1.0 - A_exp_[0] - A_exp_[1];
        }
    }

    cout << " A_exp:    " << std::fixed << std::setprecision(5);
    for (int i = 0; i < N3; ++i) {
        cout <<  std::setw(10) << A_exp_[i];
    } cout << endl;
    cout << " alpha_exp:" << std::fixed << std::setprecision(5);
    for (int i = 0; i < N3; ++i) {
        cout  << std::setw(10) << alpha_exp_[i];
    } cout << endl << endl;
//----------------------------------------------------------------------------

//------------------Shells--parameters----------------------------------------

    key = ReadInt(file,"Shells", 0, N_shells_);
    if (key != 0){ cout << "Error read shells" << endl; exit(0);}
    cout << " Shells number = " << N_shells_ << endl;

    check_v_Z_alpha_shells_();
    allocate_N_C123_I_u_Zeff_();

    Nel_tot_ = 0;
    cout << "   N        C1     C2    C3       I[eV]";
    cout << "     I[au]      u      Neff" << endl;
    const int Plen = 4;
    for (int k = 0; k < N_shells_; ++k) {

        key = ReadReal(file,"Shells", k * Plen + 1, Nel_[k]);

        if (key != 0 || Nel_[k] < 1e-6) {
            cout << endl << "Error read N for shell_k = " << k + 1 << endl;
            exit(0);
        }
        cout << std::fixed << setprecision(1) << std::setw(5) << Nel_[k];
        Nel_tot_ += Nel_[k];

//        // C1    // reading C1 is not necessarily
//        key = ReadReal(file, "Shells", k * Plen + 2, C1_[k]);
//        if (key != 0 || C1_[k] < 1e-6) {
//            cout << endl << "Error read C1 k = " << k << endl;
//            exit(0);
//        }
        // C2
        key = ReadReal(file, "Shells", k * Plen + 2, C2_[k]);
        if (key != 0 || C2_[k] < 1e-6) {
            cout << endl << "Error read C2 k = 0" << k << endl;
            exit(0);
        }
        // C3
        key = ReadReal(file, "Shells", k * Plen + 3, C3_[k]);
        if (key != 0 || C3_[k] < 1e-6) {
            cout << endl << "Error read C3 k=" << k << endl;
            exit(0);
        }
        // Calculates C1 by means of C2 and C3.
        calculates_C1_from_C2C3_(k);

        cout << std::fixed << setprecision(3) << setw(11) << C1_[k];
        cout << std::fixed << setprecision(1) << setw(5)  << C2_[k];
        cout << std::fixed << setprecision(3) << setw(8)  << C3_[k];

        key = ReadReal(file, "Shells", k * Plen + 4, I_[k]);

        if (key != 0 || I_[k] < 1e-6) {

            cout << endl << "Error read I_k at k = " << k << endl;
            exit(0);
        }
        cout << std::fixed << setprecision(2) << setw(11) << I_[k];
        cout << std::fixed << setprecision(2) << setw(10) << I_[k]/(2.0*Ebohr_);


        u_[k] = sqrt(I_[k]/Ebohr_);
        cout << std::fixed << setprecision(2) << setw(8) << u_[k];

        setup_Zeff_(k);
        if (Vi_ < u_[k]) {
            cout << std::fixed << setprecision(4) << setw(9) << Zeff_[k];
        } else {
             cout << setw(8) << "----";
        }
        cout << endl;
    }
//----------------------------------------------------------------------------

//------------------System--parameters----------------------------------------
    key = ReadReal(file, "ksmear", 0, ksmear_);
    if (key != 0){ cout << "Error read ksmear" << endl; exit(0);}
    cout << endl << " ksmear = " << ksmear_;
//----------------------------------------------------------------------------
    cout << endl;

//-----------------Cross2D--parameters----------------------------------------

    std::cout.unsetf(std::ios::floatfield);
    key = ReadReal(file,"maxvol", 0, maxvol_tolerance_);
    if (key != 0){ cout << "Error read maxvol tolerance" << endl; exit(0);}
    cout << " Maxvol tolerance = " << maxvol_tolerance_ << endl;

    key = ReadReal(file,"cross2d", 0, cross_error_);
    if (key != 0){ cout << "Error read cross2d error" << endl; exit(0);}
    cout << " Cross2D truncation error = " << cross_error_ << endl;

    key = ReadReal(file,"gauss_cut", 0, gauss_cutoff_);
    if (key != 0){ cout << "Error read gauss_cut" << endl; exit(0);}
    cout << " Gauss truncation error = " << gauss_cutoff_ << endl;
    cout << std::fixed;

    // x-range [ax,bx] Nx
    key = ReadReal(file,"x_space", 0, bx_right_bound_);
    if (key != 0){ cout << "Error read x_space 1" << endl; exit(0);}
    ax_left_bound_ = -bx_right_bound_;

    key = ReadInt(file,"x_space", 1, Nx_);
    if (key != 0){ cout << "Error read x_space 3" << endl; exit(0);}
    // make an even number of points
    if (Nx_ % 2) {++Nx_;}

    // y-range [ay,by] Ny
    key = ReadReal(file,"y_space", 0, by_right_bound_);
    if (key != 0){ cout << "Error read y_space 1" << endl; exit(0);}
    ay_left_bound_ = -by_right_bound_;
    key = ReadInt(file,"y_space", 1, Ny_);
    if (key != 0){ cout << "Error read y_space 3" << endl; exit(0);}
    if (Ny_ % 2) {++Ny_;}

//----------------------------------------------------------------------------
// Rho gauss parameters

    key = ReadInt(file, "tk_mesh", 0, tk_Npoints_);
    if (key != 0){ cout << "Error read tk_mesh parameter 0" << endl; exit(0);}
    if (tk_Npoints_ % 2) {++tk_Npoints_;}

    key = ReadReal(file, "tk_mesh", 1, tk_minus_infty_);
    if (key != 0){ cout << "Error read tk_mesh parameter 1" << endl; exit(0);}

    key = ReadReal(file, "tk_mesh", 2, tk_plus_infty_);
    if (key != 0){ cout << "Error read tk_mesh parameter 2" << endl; exit(0);}

    cout << " tk_mesh: " << tk_Npoints();
    cout << "  [" << setprecision(2) << tk_minus_infty();
    cout << ", " << tk_plus_infty() << "]" << endl << endl;
//----------------------------------------------------------------------------
// First ionization potential
    key = ReadReal(file, "Sigma_m_fold", 1, I1_first_);
    if (key != 0 || I1_first_ < 1e-12) {
        cout << endl << "Parser: Can not read Sigma_m_fold" << endl;
        exit(0);
    }
    I1_first_ /= (2.0 * Ebohr());   // transform to atomic units
//----------------------------------------------------------------------------
}

void DepositParser::check_v_Z_alpha_shells_() const {

    if (Vi_ < eps14) {
        cout << "Vi = " << Vi_ << " is very small" << endl;
        exit(0);
    }

    if (Za_ < 1 || Za_ > 120) {
        cout << "Error in Za = "  << Za_  << endl;
        exit(0);
    }

    if (N_shells_ < 1) {
        cout << "Error in shells = " << N_shells_ << endl;
        exit(0);
    }
}

void DepositParser::allocate_N_C123_I_u_Zeff_() {

    Nel_  = new double[N_shells_];
    C1_   = new double[N_shells_];
    C2_   = new double[N_shells_];
    C3_   = new double[N_shells_];
    I_    = new double[N_shells_];
    Zeff_ = new double[N_shells_];
    u_    = new double[N_shells_];
}

void DepositParser::setup_Zeff_(const int k) {

    if (Ra_ < eps14) {
        cout << "Incorrect Ra value;  Ra = " << Ra_ << endl;
        exit(0);
    }

    const double tR_gamma = C2_[k]/u_[k];
    double eta = tR_gamma/Ra_;

    if (tR_gamma > Ra_) {
        Zeff_[k] = Za_;
    } else {
        Zeff_[k] = Za_ * eta * eta;
    }
}

void DepositParser::calculates_C1_from_C2C3_(const int k) {

    const double dmup1 = 2.0 * C2_[k] + 1.0;
    C1_[k] = sqrt(pow(2.0 * C3_[k], dmup1)/alglib::gammafunction(dmup1));
}

//void DepositParser::read_ionization_potential_() {

//   !! key = ReadInt(file, "Sigma_m_fold", 0, Sigma_m_fold_);
//   !! if (key != 0){ cout << "Error read tk_mesh parameter 0" << endl; exit(0);}

//   !! key = ReadReal(file, "tk_mesh", 1, tk_minus_infty_);
//   !! if (key != 0){ cout << "Error read tk_mesh parameter 1" << endl; exit(0);}

//}

///////////////////////////////////////////////////////////////////////////////

// End of the file

