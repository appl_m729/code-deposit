#ifndef _CROSS_2D_H_
#define _CROSS_2D_H_

const int MAX_MATRIX_VALUE = 1000;

///////////////////////////////////////////////////////////////////////////////

/*! Maxvol subroutine.
 *  A_in     - input matrix of size n x r.
 *  Ind_out  - array of maxvol indeces of size r.
 *  nswp_in  - maximum number of swapping iterations.
 *  tol_in   - accuracy threshold.
 */
extern "C" void dmaxvol_(double * A_in, int * n_in, int * r_in,
                         int * Ind_out, int * nswp_in, double * tol_in);

///////////////////////////////////////////////////////////////////////////////

/*! Given function to be approximated on the rectangular grid.
 *  Calculates value in a given point (i,j). */
class FunctionGenerator {
public:
    FunctionGenerator();
    ~FunctionGenerator();

    virtual double value(const int i, const int j) const;

    void print_Aij(bool flag = true) const;
    void print_initial_info() const;

    int  n_rows() const;   // x size of the rect
    int  m_cols() const;   // y size of the rect
    
    //bounds of the rect
    double  ax_left_bound()  const;
    double  bx_right_bound() const;
    double  ay_left_bound()  const;
    double  by_right_bound() const;

    void  set_n_rows(const int n_rows);
    void  set_m_cols(const int m_cols);
    
    void  set_ax_left_bound(const double ax);
    void  set_bx_right_bound(const double bx);
    void  set_ay_left_bound(const double ay);
    void  set_by_right_bound(const double by);

    // steps: x_(i+1) - x_(i), mesh is homogenious (constant step)
    double hx() const;
    double hy() const;

private:

    int  n_rows_;
    int  m_cols_;
    
    double ax_left_bound_;
    double bx_right_bound_;
    double ay_left_bound_;
    double by_right_bound_;
};

///////////////////////////////////////////////////////////////////////////////

/*! Calculates 2d cross aproximation in a slow way
 *  (iterative expansion + reduction). Uses fortran maxvol.  */
class Cross2D {
public:
    Cross2D();
    ~Cross2D();

    // Sets up the tolerance of the maxvol routine.
    void   setup_maxvol_tolerance(const double tolerance);

    // Target function to be approximated
    void   setup_function(FunctionGenerator * generator);

    // 2d cross expansion (O(n*r^2) complexity)
    bool   find_approximate_decomposition();

    // svd decomposition for comparision (O(n^3) complexity)
    bool   find_svd_decomposition();

    // Singular values less then epsilon are thrown out
    void   set_rank_trancation(const double epsilon);

    // Calculates approximated (i,j) - value of
    // the target function by the cross decomposition.
    // It is product of the i-th line and j-th coulumn of
    // approximant matrices. O(n) complexity.
    double approx_value(const int i, const int j);

    // Singular value from SVD decomposition
    double svd_value(const int i, const int j);

    // Output
    void   print_cross_matrix(bool flag = true);
    void   print_svd_matrix(bool flag = true);

    // 2d cross decomposition  A = U * Sigma * Vt
    // U is n x r, Vt is r x m, Sigma is diagonal, r x r.
    double U(const int i, const int k) const;
    double Sigma(const int k) const;
    double Vt(const int k, const int j) const;
    int    rank() const {return rank_;}

private:
    // Accuracy of the maxvol
    double maxvol_tolerance_;
    // provides f(i,j) and other parameters
    FunctionGenerator * generator_f_;
    // Basis
    double * Ur_;        // coulumns basis
    double * Vr_;        // rows basis
    int      rank_;      // current rank
    int      max_rank_;  // max rank
    // QR
    double * tau_qr_;        // buffer vector for Lapack QR-routine
    int      lwork_size_qr_; // size of the work array (see Lapack dgeqrf_)
    double * dwork_qr_;      // work array
    // SVD
    int      lwork_size_svd_;// buffer vector for Lapack SVD-routine
    double * dwork_svd_;     // size of the work array (see Lapack dgesvd_)

    const int iterLimit_;    // maximum value of the slow-cross-2d iterations
    // maxvol
    double * Ur_copy_;
    double * Vr_copy_;
    int * I_u_;           // maxvol indeces
    int * I_v_;           // maxvol indeces
    double * Buffer_;
    // Hats (the matrices have been got from maxvol)
    double * A_hat_;
    double * Qu_hat_;
    double * Qv_hat_;
    double * M_hat_;
    // Convergence criterion by approximant
    double * old_singular_s_;
    double * Singular_s_;
    int      old_M_rank_;
    int      M_rank_;
    //
    double   rank_trancation_epsilon_;
    // Test SVD decomposition
    double * Usvd_;
    double * Vsvd_;
    double * Ssvd_;  // singular values
    double * Asvd_;
    int      svd_rank_;

    void initial_random_(double * A, int rows_size);
    void qr_decomposition_Q_(double * A, int rows_size, int cols_size);
    void pure_copy_(double * Asource, double * A_dist, int size);
    void pure_maxvol_(double * A_crash, int rows_size, int col_rnk, int * Ind_A);
    void maxvol_x2_complement_Ur_(double * Qu_out, int rows_size, int * Ind_v);
    void maxvol_x2_complement_Vr_(double * Qu_out, int rows_size, int * Ind_v);
    
    void create_A_hat_();
    void create_Q_hat_(double * Qx_hat, double * Qx_full, int Qfull_row_size, int * Ix);
    void solve_M_hat_();
    void svd_M_approximant_();

    int    find_rank_from_Sigma_(const double * const Sigma);
    double sqrt_sum_sigma2_(const double * const Sigma);
    void   copy_old_singulars_(int K_rank);
    void   print_all_singulars_(int K_old_rank, int K_rank);

    void basis_extension_Un_dgemm_(double * U_nr, int nrows, double * Qun_rK, int K_rnk);
    void basis_extension_Vr_dgemm_(double * V_mr, int mcols, double * Qvt_rK, int K_rnk);

    double Frobenius_norm_A_Aapprox_();     // cross approximation
    double Frobenius_norm_A_Asvd_();        // svd approximation
    double Frobenius_norm_Sigma_Error_(int in_K_old_rank, int in_K_rank);
    double relative_eps(int rank_K);

    void reset_current_rank_(int RankNew);
    void reallocate_memory_(int oldSize, int newSize);
    void reallocate_with_history_(double * & Arr, int oldSize, int newSize);
    void raw_reallocate_(double * & Arr, int newSize);
    void raw_int_reallocate_(int * & Arr, int newSize);

    void clean_svd_approx_Arrays_();
    void build_Aij_svd_();
    void svd_Aij_();
    void trancate_Sigma_svd_();
    void compute_Aij_approx_();
    double svd_norm_();
};

///////////////////////////////////////////////////////////////////////////////

#endif  // _CROSS_2D_H_

// End of the file

