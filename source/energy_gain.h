#ifndef _ENERGY_GAIN_H_
#define _ENERGY_GAIN_H_

#include "ccross_2d.h"

///////////////////////////////////////////////////////////////////////////////

class DepositParser;

class EnergyGain : public FunctionGenerator {
public:

    EnergyGain();
    ~EnergyGain();

    void setup(const DepositParser * const Input_Deposit_Parser);

    void fix_shell_index(const int k_shell); // starts from 0: k = 0,1,2...
    void fix_parameter_b(const double b_in);

    virtual double value(const int i, const int j) const;

    bool  is_parser() const {return deposit_parser_;}
    const DepositParser & parser() const {return *deposit_parser_;}
    int k_shell() const {return k_shell_;}

private:
    const DepositParser * deposit_parser_;
    int    k_shell_;
    double b_;

    double yK1_(const double y) const;
    double SmearStep_(const double xx) const;
};

///////////////////////////////////////////////////////////////////////////////

#endif  // _ENERGY_GAIN_H_

// End of the file
