#include <iostream>
#include <algorithm>
#include <iomanip>
#include <cmath>
#include "ccross_2d.h"
#include "add_lapack.h"

using std::cout;
using std::endl;
using std::setprecision;
using std::setw;

///////////////////////////////////////////////////////////////////////////////

FunctionGenerator::FunctionGenerator():
n_rows_(1), m_cols_(1),
ax_left_bound_(0), bx_right_bound_(1),
ay_left_bound_(0), by_right_bound_(1) {

}

FunctionGenerator::~FunctionGenerator() {
}

// virtual
// Standart routine x^2 + y^2 in [0,1] x [0,1] square
double FunctionGenerator::value(const int i, const int j) const {
    
    const double Dx_ab = bx_right_bound_ - ax_left_bound_;
    const double Dy_ab = by_right_bound_ - ay_left_bound_;
    const double i_over_n = static_cast<double>(i) / static_cast<double>(n_rows_ - 1);
    const double j_over_m = static_cast<double>(j) / static_cast<double>(m_cols_ - 1);
    
    const double xi = ax_left_bound_ + Dx_ab * i_over_n;
    const double yj = ay_left_bound_ + Dy_ab * j_over_m;
    
//    return 1.0;
//    return (i + 1) * (j + 1);
//    return (xi + 1) * (yj + 1);
//    return xi * xi + xi * yj + 4*yj * yj;
//    return exp(xi*xi + yj*yj);
//    return 34.0 * xi * xi + 0.117 * yj * yj;
//    return 1.0/(static_cast<double>(i) + j + 1.0);
//    return 1.0/(xi + yj + 1.0);
    return exp(-sqrt(xi*xi + yj*yj));
//    return 1.0/sqrt(xi*xi + yj*yj + 1.0);
//    return (4.0 + xi * xi * xi)/(2.0 + sin(15.0 * xi * xi + 9.0 * yj * yj * yj));
}

void FunctionGenerator::print_Aij(bool flag_skip) const {

    if (!flag_skip) return;

    cout << "Matrix A values:" << endl;
    for (int i = 0; i < n_rows(); ++i) {
        for (int j = 0; j < m_cols(); ++j) {
            cout << std::setw(12) << value(i,j);
        }
        cout << endl;
    }
}

void FunctionGenerator::print_initial_info() const {

    cout << " Function Generator: " << endl;
    cout << " Domain is a [" << ax_left_bound() << ",";
    cout << bx_right_bound() << "] x [" << ay_left_bound() << ",";
    cout << by_right_bound() << "] rectangle" << endl;
    cout << " with n_x = " << n_rows() << ", m_y = " << m_cols() << " points." << endl;
}

int FunctionGenerator::n_rows() const {
    return n_rows_;
}

int FunctionGenerator::m_cols() const {
    return m_cols_;
}

double FunctionGenerator::ax_left_bound() const {
    return ax_left_bound_;
}

double FunctionGenerator::bx_right_bound() const {
    return bx_right_bound_;
}

double FunctionGenerator::ay_left_bound() const {
    return ay_left_bound_;
}

double FunctionGenerator::by_right_bound() const {
    return by_right_bound_;
}

void FunctionGenerator::set_n_rows(const int nRows) {
    n_rows_ = nRows;
}

void FunctionGenerator::set_m_cols(const int mCols) {
    m_cols_ = mCols;
}

void FunctionGenerator::set_ax_left_bound(const double ax) {
    ax_left_bound_ = ax;
}

void FunctionGenerator::set_bx_right_bound(const double bx) {
    bx_right_bound_ = bx;
}

void FunctionGenerator::set_ay_left_bound(const double ay) {
    ay_left_bound_ = ay;
}

void FunctionGenerator::set_by_right_bound(const double by) {
    by_right_bound_ = by;
}

double FunctionGenerator::hx() const {
    return (bx_right_bound() - ax_left_bound())/(n_rows() - 1);
}

double FunctionGenerator::hy() const {
    return (by_right_bound() - ay_left_bound())/(m_cols() - 1);
}

///////////////////////////////////////////////////////////////////////////////

Cross2D::Cross2D():
maxvol_tolerance_(1e-2), generator_f_(0),
Ur_(0), Vr_(0), rank_(1), max_rank_(100),
tau_qr_(0), lwork_size_qr_(0), dwork_qr_(0),
lwork_size_svd_(0), dwork_svd_(0),
iterLimit_(10), Ur_copy_(0), Vr_copy_(0),
I_u_(0), I_v_(0), Buffer_(0),
A_hat_(0), Qu_hat_(0), Qv_hat_(0), M_hat_(0),
old_singular_s_(0), Singular_s_(0),
old_M_rank_(0), M_rank_(0),
rank_trancation_epsilon_(1e-6),
Usvd_(0), Vsvd_(0), Ssvd_(0), Asvd_(0), svd_rank_(0) {
}

Cross2D::~Cross2D() {
    generator_f_ = 0;
    delete[] Ur_;
    delete[] Vr_;
    delete[] tau_qr_;
    delete[] dwork_qr_;
    delete[] dwork_svd_;
    delete[] Ur_copy_;
    delete[] Vr_copy_;
    delete[] I_u_;
    delete[] I_v_;
    delete[] Buffer_;
    delete[] A_hat_;
    delete[] Qu_hat_;
    delete[] Qv_hat_;
    delete[] M_hat_;
    delete[] old_singular_s_;
    delete[] Singular_s_;
    delete[] Usvd_;
    delete[] Vsvd_;
    delete[] Ssvd_;
    delete[] Asvd_;
}

void Cross2D::setup_function(FunctionGenerator * Generator_f) {
    generator_f_ = Generator_f;
}

void Cross2D::setup_maxvol_tolerance(const double Tolerance) {
    maxvol_tolerance_ = Tolerance;
}

bool Cross2D::find_approximate_decomposition() {

    if (!generator_f_) {
        cout << "Cross2D Error!: fgen = 0 and has not been set" << endl;
        return false;
    }
    // 0. Initial iteration
    const int n_rows = generator_f_->n_rows();
    const int m_cols = generator_f_->m_cols();
    
    reallocate_memory_(0, max_rank_);   // input: old rank, new rank
    reset_current_rank_(2);
    initial_random_(Ur_, n_rows);
    initial_random_(Vr_, m_cols);
    qr_decomposition_Q_(Ur_, n_rows, rank_);
    qr_decomposition_Q_(Vr_, m_cols, rank_);
    pure_copy_(Ur_, Ur_copy_, n_rows * rank_);
    pure_copy_(Vr_, Vr_copy_, m_cols * rank_);

    int K_rank = 0;      // rank of the SVD M_hat for a given accuracy
    int K_old_rank = 0;  // old rank to estimate convergence
    int k_iter;
    for (k_iter = 0; k_iter < iterLimit_; ++k_iter) {
//        cout << "k_iter = " << k_iter << endl;

        // 1. Expansion U and V^T;
        // 1.1 expand U via V
        pure_maxvol_(Vr_copy_, m_cols, rank_, I_v_);
        maxvol_x2_complement_Ur_(Ur_, n_rows, I_v_);
        // 1.2 orthoexpand V via U
        pure_maxvol_(Ur_copy_, n_rows, rank_, I_u_);
        maxvol_x2_complement_Vr_(Vr_, m_cols, I_u_);

        // finalize expansion
        reset_current_rank_(2 * rank_);

        // 2. Orthogonalize
        qr_decomposition_Q_(Ur_, n_rows, rank_);
        qr_decomposition_Q_(Vr_, m_cols, rank_);

        // 3. Find Approximant M_2rx2r
        //    from the equation A_hat = Qu_hat * M_hat * Qv_hat^t.
        // 3.1 maxvol
        pure_copy_(Ur_, Ur_copy_, n_rows * rank_);
        pure_maxvol_(Ur_copy_, n_rows, rank_, I_u_);
        pure_copy_(Vr_, Vr_copy_, m_cols * rank_);
        pure_maxvol_(Vr_copy_, m_cols, rank_, I_v_);
        // 3.2 Create X_hats
        create_A_hat_();
        create_Q_hat_(Qu_hat_, Ur_, n_rows, I_u_);
        create_Q_hat_(Qv_hat_, Vr_, m_cols, I_v_);
        // 3.3 Find M_hat. Two steps.
        solve_M_hat_();

        // 4. Rank trancation
        copy_old_singulars_(K_rank);
        // 4.1 SVD of M_hat to get the rank of the M_hat approximant
        // Singular_s_ - s.values, Qu_hat_, Qv_hat_ <-- is tranposed !
        svd_M_approximant_();
        // 4.2 Singular values trancation.
        K_old_rank = K_rank;
        K_rank = find_rank_from_Sigma_(Singular_s_);
        //cout << "Cross2D: K_rank = " << K_rank << endl;
        //print_all_singulars_(K_old_rank, K_rank);

        // 4.3 Proper basis extension (by proper rank)
        basis_extension_Un_dgemm_(Ur_, n_rows, Qu_hat_, K_rank);
        basis_extension_Vr_dgemm_(Vr_, m_cols, Qv_hat_, K_rank);
        reset_current_rank_(K_rank);

        // 5.   Loop break condition
        // 5.1  Norm of singular-values vector difference (old - current)
        const double exit_error_1 = Frobenius_norm_Sigma_Error_(K_old_rank, K_rank);
        const double condition_exit_1 = relative_eps(K_rank);
//        cout << "Frobenius norm singular error = " << exit_error_1 << endl;
//        cout << "Condition (relative error)    = " << condition_exit_1;
//        cout << "   " << condition_exit_1 / rank_trancation_epsilon_;
//        cout << " = norm of the approximant" << endl;
        if (exit_error_1 < condition_exit_1) {
            return true;
        }

        // 6. Check exceeding of rank condition.
        if ((2 * rank_) > std::min(n_rows, m_cols)) {
            cout << "Cross2D: Error! Rank exceeds the size of the matrix" << endl;
            cout << "Rank = " << rank_ << endl;
            return false;
        }
    }

    cout << "Cross2D: Error! Maximum iterations limit exceeded." << endl;
    cout << "Cross2D did not converge." << endl;
    cout << "k_iter = " << k_iter << endl;
    return false;
}

bool Cross2D::find_svd_decomposition() {

    if (!generator_f_) {
        return false;
    }
    clean_svd_approx_Arrays_(); // 0. Memory re-allocation
    build_Aij_svd_();           // 1. Fill matrix Aij from f(i,j)
    svd_Aij_();                 // 2. SVD
    trancate_Sigma_svd_();      // 3. Trancate Singular values
    compute_Aij_approx_();      // 4. Calculates Aij from SVD

    cout << "SVD rank = " << svd_rank_ << endl;
    cout << "Frobenius norm  SVD error  = " << Frobenius_norm_A_Asvd_() << endl;
    cout << "Condition (relative error) = ";
    cout << rank_trancation_epsilon_ * svd_norm_();
    cout << "  " << svd_norm_() << " = norm of the svd approximant" << endl;

    return true;
}

void Cross2D::set_rank_trancation(const double rtEpsilon) {
    rank_trancation_epsilon_ = rtEpsilon;
}

void Cross2D::initial_random_(double * A, int rows_size) {

    int size_nr = rows_size * rank_;
    for (int i = 0; i < size_nr; ++i) {
        A[i] = rand() % (2 * MAX_MATRIX_VALUE + 1) - MAX_MATRIX_VALUE;
    }
}

double Cross2D::approx_value(const int i, const int j) {

    const int n_rows = generator_f_->n_rows();
    const int m_cols = generator_f_->m_cols();

    double sum_apprx = 0;
    for (int k = 0; k < rank_; ++k) {
        sum_apprx += Ur_[i + n_rows * k] * Singular_s_[k] * Vr_[j + m_cols * k];
    }
    return sum_apprx;
}

double Cross2D::U(const int i, const int k) const {
    const int n_rows = generator_f_->n_rows();
    return Ur_[i + n_rows * k];
}

double Cross2D::Sigma(const int k) const {
    return Singular_s_[k];
}

double Cross2D::Vt(const int k, const int j) const {
    const int m_cols = generator_f_->m_cols();
    return Vr_[j + m_cols * k];
}

double Cross2D::svd_value(const int i, const int j) {

    int nrows = generator_f_->n_rows();
    int mcols = generator_f_->m_cols();
    int svd_rank = std::min<int>(nrows, mcols);

    double sum_ij = 0;
    for (int k = 0; k < svd_rank_; ++k) {
        sum_ij += Usvd_[i + nrows * k] * Ssvd_[k] * Vsvd_[k + svd_rank * j];
    }
    return sum_ij;
}

void Cross2D::print_cross_matrix(bool flag_skip) {

    if (!flag_skip) return;

    cout << endl << " Cross approximation: " << endl;
    for (int i = 0; i < generator_f_->n_rows(); ++i) {
        for (int j = 0; j < generator_f_->m_cols(); ++j) {
                cout << std::setw(12) << approx_value(i,j);
        }
        cout << endl;
    }
}

void Cross2D::print_svd_matrix(bool flag_skip) {

    if (!flag_skip) return;

    const int nrows = generator_f_->n_rows();
    const int mcols = generator_f_->m_cols();

    cout << endl << "SVD back product:" << endl;
    for (int i = 0; i < nrows; ++i) {
        for (int j = 0; j < mcols; ++j) {
            cout << std::setw(12) << Asvd_[i + nrows * j];
        }
        cout << endl;
    }
}

void Cross2D::qr_decomposition_Q_(double * A, int n_rows, int m_cols) {

    int info;
    // calculates proper size of the work_out
    int negative_work = -1;
    dgeqrf_(&n_rows, &m_cols, A, &n_rows, tau_qr_, dwork_qr_, &negative_work, &info);

    // reallocates if needed
    if (dwork_qr_[0] > lwork_size_qr_) {
        lwork_size_qr_ = dwork_qr_[0];
        delete[] dwork_qr_;
        dwork_qr_ = new double[lwork_size_qr_];
    }
    // QR decomposition
    dgeqrf_(&n_rows, &m_cols, A, &n_rows, tau_qr_, dwork_qr_, &lwork_size_qr_, &info);
    // There is Q in matrix Ur_
    int k_refl = m_cols;
    dorgqr_(&n_rows, &m_cols, &k_refl, A, &n_rows, tau_qr_, dwork_qr_, &lwork_size_qr_, &info);
}

void Cross2D::pure_copy_(double * Asource, double * A_dist, int size) {
    int xInc = 1;
    dcopy_(&size, Asource, &xInc, A_dist, &xInc);
}

void Cross2D::pure_maxvol_(double * A_crash, int rows_size, int col_rnk, int * tInd) {

    int nswap = 1024;
    double tolerance = maxvol_tolerance_;
    dmaxvol_(A_crash, &rows_size, &col_rnk, tInd, &nswap, &tolerance);
}

void Cross2D::maxvol_x2_complement_Ur_(double * Uq, int rows_sz, int * Ind_v) {

    const int shift_nr = rows_sz * rank_;
    for (int k = 0; k < rank_; ++k) {
        const int k_shift = rows_sz * k;
        for (int i = 0; i < rows_sz; ++i) {
            Uq[shift_nr + i + k_shift] = generator_f_->value(i, Ind_v[k] - 1);
        }
    }
}

void Cross2D::maxvol_x2_complement_Vr_(double * Vq, int cols_sz, int * Ind_u) {

    const int shift_mr = cols_sz * rank_;
    for (int k = 0; k < rank_; ++k) {
        const int k_shift = cols_sz * k;
        for (int j = 0; j < cols_sz; ++j) {
            Vq[shift_mr + j + k_shift] = generator_f_->value(Ind_u[k] - 1, j);
        }
    }
}

void Cross2D::create_A_hat_() {

    for (int i = 0; i < rank_; ++i) {
        for (int j = 0; j < rank_; ++j) {
            // natural matrix
            // A_hat_[i + j * rank_] = generator_f_->value(I_u_[i] - 1, I_v_[j] - 1);
            // transposed matrix (need for the Qv * X = A^t equation)
            A_hat_[j + i * rank_] = generator_f_->value(I_u_[i] - 1, I_v_[j] - 1);
        }
    }
}

void Cross2D::create_Q_hat_(double * Qx_hat, double * Qx_full, int row_sz, int * Ix) {

    for (int i = 0; i < rank_; ++i) {
        for (int k = 0; k < rank_; ++k) {
            Qx_hat[i + k * rank_] = Qx_full[(Ix[i] - 1) + k * row_sz];
        }
    }
}

void Cross2D::solve_M_hat_() {

    int r_sz = rank_;
    int info;

    // 1) Qv * X = A^t;   A_hat have been transposed already
    dgesv_(&r_sz, &r_sz, Qv_hat_, &r_sz, I_u_, A_hat_, &r_sz, &info);

    // 2) Qu * M = X^t;   X = A_hat
    // 2.1  Transpose A_hat  (Preliminary step)
    for (int i = 0; i < r_sz; ++i) {
        for (int j = i; j < r_sz; ++j) {
            const double a_ij = A_hat_[i + r_sz * j];
            const double a_ji = A_hat_[j + r_sz * i];
            A_hat_[i + r_sz * j] = a_ji;
            A_hat_[j + r_sz * i] = a_ij;
        }
    }

    // 2.2 Solve Qu * M = X^t;           I_u_ is just a dummy variable
    dgesv_(&r_sz, &r_sz, Qu_hat_, &r_sz, I_u_, A_hat_, &r_sz, &info);
    // Now M_hat is in A_hat
}

void Cross2D::svd_M_approximant_() {

    char U_mode  = 'A';
    char Vt_mode = 'A';
    int r_sz = rank_;
    int info;

    dgesvd_(&U_mode, &Vt_mode, &r_sz, &r_sz, A_hat_,
            &r_sz, Singular_s_, Qu_hat_, &r_sz, Qv_hat_, &r_sz,
            dwork_svd_, &lwork_size_svd_, &info);
}

int Cross2D::find_rank_from_Sigma_(const double * const Sigma) {

    int r_sz = rank_;
    double full_sum = 0;
    for (int k = r_sz - 1; k >= 0; --k) {
        const double sigm_i = Sigma[k];
        full_sum += sigm_i * sigm_i;
    }

    int Kr = 0;
    double partial_sum = 0;
    for (Kr = r_sz - 1; Kr >= 0; --Kr) {
        const double sigm_i = Sigma[Kr];
        partial_sum += sigm_i * sigm_i;

        const double sigma_ratio = sqrt(partial_sum/full_sum);

        if (sigma_ratio > rank_trancation_epsilon_) {
            break;
        }
    }
    return Kr + 1;
}

double Cross2D::sqrt_sum_sigma2_(const double * const Sigma) {

    int r_sz = rank_;
    double sum = 0;
    for (int k = 0; k < r_sz; ++k) {
        const double sgm_k = Sigma[k];
        sum += sgm_k * sgm_k;
    }
    return sqrt(sum);
}

void Cross2D::copy_old_singulars_(int K_in_rank) {

    for (int k = 0; k < K_in_rank; ++k) {
        old_singular_s_[k] = Singular_s_[k];
    }
}

void Cross2D::print_all_singulars_(int K_old_rank, int K_rank) {

    cout << "Singular values (old):" << endl;
    for (int i = 0; i < K_old_rank; ++i) {
        cout << setw(14) << old_singular_s_[i];
        if ( !((i + 1) % 5) ) {
            cout << endl;
        }
    } cout << endl;

    cout << "Singular values (trancated):" << endl;
    for (int i = 0; i < K_rank; ++i) {
        cout << setw(14) << Singular_s_[i];
        if ( !((i + 1) % 5) ) {
            cout << endl;
        }
    } cout << endl;

    cout << "Singular values (full rank):" << endl;
    for (int i = 0; i < rank_; ++i) {
        cout << setw(14) << Singular_s_[i];
        if ( !((i + 1) % 5) ) {
            cout << endl;
        }
    } cout << endl;
}

void Cross2D::basis_extension_Un_dgemm_(double * U_nr, int nrows, double * Qun_rK, int K_rnk) {

    char trans_A = 'N';  // Is A transposed in dgemm or not
    char trans_B = 'N';  // Is B ...
    double alpha = 1.0;  // C = alpha * A * B + beta * C
    double beta  = 0.0;
    int m_rows = nrows;  // rows of matrix A
    int n_cols = K_rnk;  // columns of matrix C
    int k_cols = rank_;  // rows of matrix B == columns of matrix A

    dgemm_(&trans_A, &trans_B, &m_rows, &n_cols, &k_cols, &alpha,
            U_nr, &m_rows, Qun_rK, &k_cols, &beta, Ur_copy_, &m_rows);

    int size2nk = nrows * K_rnk;
    pure_copy_(Ur_copy_, Ur_, size2nk);
}

void Cross2D::basis_extension_Vr_dgemm_(double * V_mr, int mcols, double * Qvt_rK, int K_rnk) {

    char trans_A = 'N';  // Is A transposed in dgemm or not
    char trans_B = 'T';  // Is B transposed
    double alpha = 1.0;  // C = alpha * A * B^T + beta * C
    double beta  = 0.0;
    int m_rows = mcols;  // rows of matrix A
    int n_cols = K_rnk;  // columns of matrix C
    int k_cols = rank_;  // rows of matrix B == columns of matrix A

    dgemm_(&trans_A, &trans_B, &m_rows, &n_cols, &k_cols, &alpha,
            V_mr, &m_rows, Qvt_rK, &k_cols, &beta, Vr_copy_, &m_rows);

    int size2nk = mcols * K_rnk;
    pure_copy_(Vr_copy_, Vr_, size2nk);
}

double Cross2D::Frobenius_norm_A_Aapprox_() {

    int n_rows = generator_f_->n_rows();
    int m_cols = generator_f_->m_cols();

    double sum2_aa = 0.0;
    for (int i = 0; i < n_rows; ++i) {
        for (int j = 0; j < m_cols; ++j) {

            const double delta_ij = generator_f_->value(i,j) - approx_value(i,j);
            sum2_aa += delta_ij * delta_ij;
        }
    }

    return sqrt(sum2_aa);
}

double Cross2D::Frobenius_norm_A_Asvd_() {

    int n_rows = generator_f_->n_rows();
    int m_cols = generator_f_->m_cols();

    double sum2_aa = 0.0;
    for (int i = 0; i < n_rows; ++i) {
        for (int j = 0; j < m_cols; ++j) {

            const double delta_ij = generator_f_->value(i,j) - Asvd_[i + n_rows * j];
            sum2_aa += delta_ij * delta_ij;
        }
    }

    return sqrt(sum2_aa);
}

double Cross2D::Frobenius_norm_Sigma_Error_(int old_rank_K, int rank_K) {

    // calculate differences
    double sum_diff = 0;
    for (int k = 0; k < old_rank_K; ++k) {
        const double delta_ss = Singular_s_[k] - old_singular_s_[k];
        sum_diff += delta_ss * delta_ss;
    }
    // calculate tail
    double sum_tail = 0;
    for (int k = rank_K - 1; k >= old_rank_K; --k) {
        sum_tail += Singular_s_[k] * Singular_s_[k];
    }
    return sqrt(sum_diff + sum_tail);
}

double Cross2D::relative_eps(int rank_K) {

    double sum = 0;
    for (int k = rank_K; k >= 0; --k) {
        sum += Singular_s_[k] * Singular_s_[k];
    }
    return sqrt(sum) * rank_trancation_epsilon_;
}

void Cross2D::reset_current_rank_(int RankNew) {

    // Message
//    cout << "Cross2D: reset rank = " << rank_;
//    cout << " to in_rank = " << RankNew << endl;
    
    if (RankNew > max_rank_) {
    // increase max_rank
        max_rank_ *= 2;
        if (max_rank_ < RankNew) {
            max_rank_ = RankNew + 1;
        }
        reallocate_memory_(rank_, RankNew);   // old rank, new rank
    }
    rank_ = RankNew;
//    cout << "Cross2D: new_rank = " << rank_ << endl;
}

void Cross2D::reallocate_memory_(int oldSize, int newSize) {
    
    // Message
//    cout << "Cross2D: memory reallocation from old_size = ";
//    cout << oldSize << "  to new_size = " << newSize << endl;
    
    // Ur
    int Ur_old_size = generator_f_->n_rows() * oldSize;
    int Ur_new_size = generator_f_->n_rows() * newSize;
    // Vr
    int Vr_old_size = generator_f_->m_cols() * oldSize;
    int Vr_new_size = generator_f_->m_cols() * newSize;

    // reallocate
    raw_reallocate_(Buffer_, std::max<int>(Ur_new_size, Vr_new_size));
    reallocate_with_history_(Ur_, Ur_old_size, Ur_new_size);
    reallocate_with_history_(Vr_, Vr_old_size, Vr_new_size);

    // QR
    raw_reallocate_(tau_qr_, newSize);
    lwork_size_qr_ = max_rank_ * 8; // factor 8 just a guess.
    raw_reallocate_(dwork_qr_, lwork_size_qr_);

    // SVD
    // for better performance --> 2 * 5 <-- read lapack svd description
    lwork_size_svd_ = 10 * max_rank_;
    raw_reallocate_(dwork_svd_, lwork_size_svd_);

    // maxvol
    raw_reallocate_(Ur_copy_, Ur_new_size);
    raw_reallocate_(Vr_copy_, Vr_new_size);
    raw_int_reallocate_(I_u_, newSize);
    raw_int_reallocate_(I_v_, newSize);

    // hats
    raw_reallocate_(A_hat_,  max_rank_ * max_rank_);
    raw_reallocate_(Qu_hat_, max_rank_ * max_rank_);
    raw_reallocate_(Qv_hat_, max_rank_ * max_rank_);
    raw_reallocate_(M_hat_,  max_rank_ * max_rank_);

    // Convergence criterion by approximant
    reallocate_with_history_(Singular_s_, oldSize, newSize);
    reallocate_with_history_(old_singular_s_, oldSize, newSize);
}

void Cross2D::reallocate_with_history_(double * & Arr, int oldSize, int newSize) {

    int xInc = 1;
    dcopy_(&oldSize, Arr, &xInc, Buffer_, &xInc);
    raw_reallocate_(Arr, newSize);
    dcopy_(&oldSize, Buffer_, &xInc, Arr, &xInc);
}

void Cross2D::raw_reallocate_(double *  &Arr, int newSize) {

    delete[] Arr;
    Arr = new double[newSize];
}

void Cross2D::raw_int_reallocate_(int * & iArr, int newSize) {

    delete[] iArr;
    iArr = new int[newSize];
}

void Cross2D::clean_svd_approx_Arrays_() {

    delete[] Usvd_; Usvd_ = 0;
    delete[] Vsvd_; Vsvd_ = 0;
    delete[] Ssvd_; Ssvd_ = 0;
    delete[] Asvd_; Asvd_ = 0;

    const int nm_size = generator_f_->n_rows() * generator_f_->m_cols();
    svd_rank_ = std::min<int>(generator_f_->n_rows(), generator_f_->m_cols());

    Asvd_ = new double[nm_size];
    Usvd_ = new double[nm_size];
    Vsvd_ = new double[nm_size];
    Ssvd_ = new double[svd_rank_];
}

void Cross2D::build_Aij_svd_() {

    const int nrows = generator_f_->n_rows();
    const int mcols = generator_f_->m_cols();

    for (int i = 0; i < nrows; ++i) {
        for (int j = 0; j < mcols; ++j) {
            Asvd_[i + nrows * j] = generator_f_->value(i,j);
        }
    }
}

void Cross2D::svd_Aij_() {

    char U_mode  = 'S';
    char Vt_mode = 'S';
    int nrows = generator_f_->n_rows();
    int mcols = generator_f_->m_cols();
    int svd_rank = std::min<int>(nrows, mcols);
    int lsvd_work = 10 * std::max<int>(nrows, mcols);
    double * svd_dwork = new double[lsvd_work];
    int info;

    dgesvd_(&U_mode, &Vt_mode, &nrows, &mcols, Asvd_, &nrows, Ssvd_,
            Usvd_, &nrows, Vsvd_, &svd_rank, svd_dwork, &lsvd_work, &info);

    cout << "Pure SVD values:" << endl;
    for (int i = 0; i < svd_rank; ++i) {
        cout << std::setw(14) << Ssvd_[i];
        if ( !((i + 1) % 5) ) {
            cout << endl;
        }
    }
    cout << endl;

    delete[] svd_dwork;
}

void Cross2D::trancate_Sigma_svd_() {

    int nrows = generator_f_->n_rows();
    int mcols = generator_f_->m_cols();
    svd_rank_ = std::min<int>(nrows, mcols);

    // Trancation
    const double relative_condition = rank_trancation_epsilon_ * svd_norm_();
    for (int kr = 0; kr < svd_rank_; ++kr) {
        if (Ssvd_[kr] < relative_condition) {
            svd_rank_ = kr;
            return;
        }
    }
}

void Cross2D::compute_Aij_approx_() {

    int nrows = generator_f_->n_rows();
    int mcols = generator_f_->m_cols();

    for (int i = 0; i < nrows; ++i) {
        for (int j = 0; j < mcols; ++j) {
            Asvd_[i + nrows * j] = svd_value(i,j);
        }
    }
}

double Cross2D::svd_norm_() {

    double sum_norm = 0;
    for (int kr = svd_rank_ - 1; kr >= 0; --kr) {
        const double sigma_k = Ssvd_[kr];
        sum_norm += sigma_k * sigma_k;
    }
    return sqrt(sum_norm);
}

///////////////////////////////////////////////////////////////////////////////

// End of the file

