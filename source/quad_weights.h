#ifndef _QUADRATURE_WEIGHTS_H_
#define _QUADRATURE_WEIGHTS_H_

///////////////////////////////////////////////////////////////////////////////

class QuadWeights {
public:
    QuadWeights();
    virtual ~QuadWeights();

    void set_size(const int N);
    virtual void setup_weights();

    int N() const;
    double w(const int i) const;

protected:

    bool     N_flag_ok_;
    int      N_;
    double * w_;
};


class TrapezoidWeights : public QuadWeights {
public:
    TrapezoidWeights();
    ~TrapezoidWeights();

    virtual void setup_weights();
};


class SimpsonWeights : public QuadWeights {
public:
    SimpsonWeights();
    ~SimpsonWeights();

    virtual void setup_weights();
};

///////////////////////////////////////////////////////////////////////////////

#endif // _QUADRATURE_WEIGHTS_H_

// End of the file

