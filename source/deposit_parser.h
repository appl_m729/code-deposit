#ifndef _DEPOSIT_PARSER_H_
#define _DEPOSIT_PARSER_H_

///////////////////////////////////////////////////////////////////////////////

const int N3  = 3;           // number of parameters alpha and A
const double eps14 = 1e-14;  // accuracy for ...

/*! This class reads input data from the input file. */

class DepositParser {
public:
    DepositParser();
    ~DepositParser();

    // Read input parameters
    void  setup(const char file[]);

    // Target atom data
    double  Za()    const {return Za_;}
    double  Ra()    const {return Ra_;}
    double  Ebohr() const {return Ebohr_;}     // 13.60569253
    // Dirac-Fock parameters of the atomic (target) potential.
    double  A_exp(const int i)     const {return A_exp_[i];}
    double  alpfa_exp(const int i) const {return alpha_exp_[i];}

    // Projectile data
    double  Vi()             const {return Vi_;}
    int     N_shells()       const {return N_shells_;}  // number of shells
    double  Nel_total()      const {return Nel_tot_;}   // number of electrons
    double  Nel(const int k) const {return Nel_[k];}    // in k-th shell

    // Slater parameters
    double C1(const int k)   const {return C1_[k];}
    double C2(const int k)   const {return C2_[k];}
    double C3(const int k)   const {return C3_[k];}
    double I (const int k)   const {return I_[k];}
    double Zeff(const int k) const {return Zeff_[k];}
    double u (const int k)   const {return u_[k];}
    double k_smear()         const {return ksmear_;}

    // m-fold: First ionization potential
    double I1_first_au() const {return I1_first_;}

    // Cross2D parameters
    double maxvol_tolerance() const {return maxvol_tolerance_;}
    double ax_left_bound()  const {return ax_left_bound_; }
    double bx_right_bound() const {return bx_right_bound_;}
    double ay_left_bound()  const {return ay_left_bound_; }
    double by_right_bound() const {return by_right_bound_;}
    int    Nx() const {return Nx_;}      // Absolute value for the x-grid points
    int    Ny() const {return Ny_;}
    int    Nx_points() const {return 2 * Nx_ + 1;}      // Number of grid points
    int    Ny_points() const {return 2 * Ny_ + 1;}
    double cross_error()  const {return cross_error_;}  // See private block
    double gauss_cutoff() const {return gauss_cutoff_;}

    // b_min = 0;  b_max = y_max
    double azeta_left_bound()  const {return -2.0 * by_right_bound_;}
    double bzeta_right_bound() const {return by_right_bound_;}
    int    Nzeta_points() const {return 3 * Ny_ + 1;}

    // step of the uniform grid
    double hx() const {return (bx_right_bound() - ax_left_bound())/(Nx_points() - 1);}
    double hy() const {return (by_right_bound() - ay_left_bound())/(Ny_points() - 1);}

    // Rho Gauss mesh tk
    double tk_minus_infty() const {return tk_minus_infty_;}  // range [t1, t2]
    double tk_plus_infty() const  {return tk_plus_infty_;}
    int    tk_Npoints() const     {return tk_Npoints_;}      // number of points in the range [t1, t2]

private:
    double  Vi_;         // Velocity of ion
    double  Za_;         // Charge of the target atom
    double  Ra_;         // Averaged radius of the target atom
    const double Ebohr_; // 13.6...
    double  A_exp_[N3], alpha_exp_[N3];   // Dirac-HF parameters,
    int     N_shells_;                    // number of shells

    int      Nel_tot_;  // total number of electrons of the projectile
    double * Nel_;      // Numb of equiv. electrons on the given shell
    double * C1_;       // Norm factors of Slater function. Array.
    double * C2_;       // r^C2 in Slater function         ( C2 = mu   )
    double * C3_;       // exp(-C3 * r) in Slater function ( C3 = beta )
    double * I_;        // I - binding energy
    double * Zeff_;     // Effective charge, depends on r_gamma
    double * u_;        // sqrt(I/_Ebohr)
    double   ksmear_;   // Energy smearing parameter

    double   I1_first_; // first ionization potential

    // Cross parameters
    double maxvol_tolerance_;  // 1e-2. Is used in maxvol.
    double ax_left_bound_;      // [ax, bx]
    double bx_right_bound_;
    double ay_left_bound_;      // [ay, by]
    double by_right_bound_;
    int    Nx_;                 // number of points in [ax, bx]
    int    Ny_;                 // --  --  --  --  --  [ay, by]
    double cross_error_;        // Singular values trancation
    double gauss_cutoff_;       // Slater via Gaussians expansion

    // Slater density via Gaussian expansion
    double tk_minus_infty_;     // [t_start,
    double tk_plus_infty_;      //           t_end]
    int    tk_Npoints_;         // Ntk

    void   check_v_Z_alpha_shells_() const; // Checks validity of input parameters
    void   allocate_N_C123_I_u_Zeff_();     // Allocates memory for shells structures
    void   setup_Zeff_(const int k);        // Effective charge
    void   calculates_C1_from_C2C3_(const int k);
//    void   read_ionization_potential_();
};

///////////////////////////////////////////////////////////////////////////////

#endif   // _DEPOSIT_PARSER_H_

// End of the file

