#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include "probability_m.h"
#include "integral_tensor_sum.h"
#include "deposit_parser.h"
#include "read.h"

using namespace::std;

////////////////////////////////////////////////////////////////////////////

Probability_mfold::Probability_mfold():
OVERFLOW_CONST(1e+20),
ZERO_CONST(1e-12),
integral_lr_Tb_(0),
d_parser_(0),
m_fold_max_(0),
Im_(0),
eta_(0),
Pm_(0),
l_tot_(0),
b_(0),
Tb_(0) {

}

Probability_mfold::~Probability_mfold() {

    integral_lr_Tb_ = 0;
    d_parser_       = 0;

    deallocate_Im_eta_();
    delete[] Pm_;  Pm_ = 0;

    b_  = 0;
    Tb_ = 0;
}

void Probability_mfold::set_parser(const DepositParser * const Dpst_Parser) {

    if (!Dpst_Parser) {
        cout << "Error in Probability_mfold::set_parser(" << endl;
        cout << "The parser pointer is zero" << endl;
        exit(0);
    }
    d_parser_ = Dpst_Parser;
}

void Probability_mfold::set_integral_Tb(const IntegralTb_TensorSum * Integral_lr_Tb) {

    if (!Integral_lr_Tb) {

        cout << "Error in Probability_mfold::set_integral_Tb(" << endl;
        cout << "The integral Tb pointer is zero." << endl;
        exit(0);
    }

    integral_lr_Tb_ = Integral_lr_Tb;
}

// Copy hashed arrays
void Probability_mfold::set_b_Tb_array(const double * const ext_b,
                                       const double * const ext_Tb,
                                       const int ext_l_tot) {
    // init value
    l_tot_ = ext_l_tot;

    // init pointer adresses
    b_  = ext_b;
    Tb_ = ext_Tb;
}

// read Ionization potentials
bool Probability_mfold::read(const char file[]) {

    if ( !d_parser_ ) return false;
    if ( !integral_lr_Tb_ ) return false;

    int key = ReadInt(file, "Sigma_m_fold", 0, m_fold_max_);
    if (key != 0 || m_fold_max_ < 1 || m_fold_max_ > 101) {
        return false;
    }

    deallocate_Im_eta_();
    allocate_Im_eta_(m_fold_max_);

    for (int m = 0; m < m_fold_max_; ++m) {

        key = ReadReal(file,"Sigma_m_fold", 1 + m, Im_[m]);

        if (key != 0 || Im_[m] < 0.0) {
            return false;
        }

        Im_[m] /= (2.0 * d_parser_->Ebohr());
        eta_[m] = 0.0;
    }

    return true;
}

void Probability_mfold::setup() {

    if (!b_ || !Tb_) {
        cout << "Tb and b arrays did not setup. Zero pointers Error." << endl;
        exit(0);
    }
    if (l_tot_ < 4) {
        cout << "Error in Probability_mfold::calculate(" << endl;
        cout << "too low l_tot = " << l_tot_ << endl;
        //exit(0);
    }
    if (!integral_lr_Tb_) {
        cout << "Error in Probability_mfold::setup(" << endl;
        cout << "The Integral Tb pointer is zero" << endl;
        //exit(0);
    }

    if (Pm_) {
        delete[] Pm_;  Pm_ = 0;
    }
    Pm_ = new double[(l_tot_ + 1) * m_fold_max_];

    // Zero Pm
    for (int mm = 0; mm < m_fold_max_; ++mm) {
        // clear Pm
        set_in_zero_P_(mm);
    }
    // setup Pm
    for (int m = 0; m < m_fold_max_; ++m) {
        calc_P_(m);
    }
}

void Probability_mfold::write(const char file1[]) const {

    ofstream fout(file1);
    
    fout.precision(16);
    fout << "b   ";
    
    // writes caption        
    for (int m = 0; m < m_fold_max(); ++m) {
        fout << " P_["<< m << "] ";
    }fout << endl;

    // writes values        
    for(int i = 0; i <= l_tot_; ++i) {
        fout << std::fixed << setprecision(6) << setw(10) << b_[i];
        for (int m = 0; m < m_fold_max(); ++m) {
            fout << std::fixed << setprecision(8) << setw(14) << P(m,i);  
        }fout << endl;
    }
    fout.close();

    cout << endl << " Output Pm(b) into the file:  " << file1 << endl;
}

double Probability_mfold::Im(const int m) const {

    if ((m < 0) || (m >= m_fold_max_)) {

        cout << "Error in Probability_mfold::Im!  wrong m = " << m << endl;
        cout << "m_fold_max = " << m_fold_max_ << endl;
        exit(0);
    }

    return Im_[m];
}

double Probability_mfold::P(const int m_fold, const int ib) const {

    if (ib < 0 || ib > l_tot_) {
        cout << "Error in  Probability_mfold::P(" << endl;
        exit(0);
    }
    return Pm_[(l_tot_ + 1) * m_fold + ib];
}

double Probability_mfold::b(const int ib) const {
  
    if (ib < 0 || ib > l_tot_) {
        cout << "Error in  Probability_mfold::b(" << endl;
        exit(0);
    }
    return b_[ib];
}

double Probability_mfold::Pm_x_b(const int m_fold, const int ib) const {

//    return b_[ib] * b_[ib];

    if (ib < 0 || ib > l_tot_) {
        cout << "Error in  Probability_mfold::Pm_x_b(" << endl;
        cout << "ib is out of range:  ib = " << ib << "  ";
        cout << "l_tot_ = " << l_tot_ << endl;
        exit(0);
    }
    return Pm_[(l_tot_ + 1) * m_fold + ib] * b_[ib];
}

double Probability_mfold::P_NmEta(const int N, const int m, const int m_Max) const {

    if (m < 1 || m > N) {

        cout << " Error in Probability_mfold::P_Nmx(" << endl;
        cout << " m = " << m << "  N = " << N << endl;
        exit(0);
    }

    // negative argument, equation (45)
    if (eta_[m-1] < ZERO_CONST) {

        if (m == 1) {return 1.0;}
        else {return 0.0;}
    }

    // equation (46)
    if (eta_[N-1] > OVERFLOW_CONST) {

        if (m == N) {return 1.0;}
        else {return 0.0;}
    }

    // equation (47)
    double p_sum = 0;
    for (int i = 1; i <= m_Max; ++i) {
    
        if (m == i) {
            p_sum += 1.0; 
        } else {
            p_sum += exp(Si_to_Sm_(N, m, i));
        }
    }

    const double rezlt(1.0 / p_sum);
    
    if (rezlt < 1e-20) {
        return 0.0;
    }
    
    return rezlt;
}

// Im, eta arrays deallocation
void Probability_mfold::deallocate_Im_eta_() {
    delete[] Im_;   Im_  = 0;
    delete[] eta_;  eta_ = 0;
}

// Im, eta arrays allocation
void Probability_mfold::allocate_Im_eta_(const int m_size) {
    Im_  = new double[m_size];
    eta_ = new double[m_size];
}

void Probability_mfold::set_in_zero_P_(const int mm) {

    const int shift_index = (l_tot_ + 1) * mm;

    for (int i = 0; i <= l_tot_; ++i) {
        Pm_[shift_index + i] = 0.0;
    }
}

void Probability_mfold::calc_P_(const int mm) {

    const int shift_index = (l_tot_ + 1) * mm;

    for (int i = 0; i <= l_tot_; ++i) {
        const int m_Max = setup_all_eta_array_(i);
        Pm_[shift_index + i] = P_NmEta(m_fold_max_, mm + 1, m_Max);
    }
}

int Probability_mfold::setup_all_eta_array_(const int i_b) {

    int    m_max  = 0;
    double Ik_sum = 0;

    for (int k = 0; k < m_fold_max_; ++k) {

        Ik_sum += Im_[k];
        eta_[k] = (Tb_[i_b] - Ik_sum) / Im_[0];

        if (eta_[k] > ZERO_CONST) {
            m_max = k;
        }
    }
    return m_max + 1;
}

double Probability_mfold::Si_to_Sm_(const int N, const int m, const int i) const {

    if (m == i) {return 0.0;}

    double sum1 = 0;
    double sum2 = 0;

    // equations (48) - (54)
    if (m < i) {
        for (double v1 = N - i + 1; v1 <= N - m; ++v1) {  
            sum1 += log(v1);
        }
        for (double v2 = m + 1; v2 <= i; ++v2) {      
            sum2 += log(v2);
        }
    } else {
        for (double v1 = N - m + 1; v1 <= N - i; ++v1) {  
            sum2 += log(v1);
        }
        for (double v2 = i + 1; v2 <= m; ++v2) {      
            sum1 += log(v2);
        }
    }

    double sum3 = 0;
    for (double v3 = 3*m - 2; v3 >= 2; v3 -= 2.0) {
        sum3 += log(v3);
    }

    double sum4 = 0;
    for (double v4 = 3*i - 2; v4 >= 2; v4 -= 2.0) {
        sum4 += log(v4);
    }

    const double sum5 = (floor(0.5 * (i - 1.0)) - floor(0.5 * (m - 1.0))) * log(2.0);
    const double sum6 = (floor(0.5 * i) - floor(0.5 * m)) * log(M_PI);
    const double sum7 = (1.5 * i - 1.0) * log(eta_[i - 1]);
    const double sum8 = (1.5 * m - 1.0) * log(eta_[m - 1]);

    return sum1 - sum2 + sum3 - sum4 + sum5 + sum6 + sum7 - sum8;  
}

////////////////////////////////////////////////////////////////////////////

// end of file
