#include <iostream>
#include <cmath>
#include <iomanip>
#include "deposit_solver_lr.h"
#include "integral_tensor_sum.h"
#include "deposit_parser.h"
#include "interpolation.h"
#include "quad_weights.h"

using std::cout;
using std::endl;
using namespace alglib;

///////////////////////////////////////////////////////////////////////////////

int main(int argc,char* argv[]) {

    if (argc == 3) {

        DepositSolverLR * const d_solver(new DepositSolverLR);
        d_solver->solve(argv[1], argv[2]);
        delete d_solver;

    } else {

        cout << " Input error!..\nTwo parameters are required ";
        cout << " input_file.txt and output_file.txt" << endl;
    }

    return 0;
}

///////////////////////////////////////////////////////////////////////////////

// End of the file

