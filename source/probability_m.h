#ifndef _PROBABLITY_M_FOLD_H_
#define _PROBABLITY_M_FOLD_H_

////////////////////////////////////////////////////////////////////////////

class IntegralTb_TensorSum;
class DepositParser;


class Probability_mfold {

public:

    Probability_mfold();
    ~Probability_mfold();

    // Parser pointer
    void   set_parser(const DepositParser * const parser);

    // Takes the integral pointer
    void   set_integral_Tb(const IntegralTb_TensorSum * EbIntgrl);

    // Copy hashed arrays
    void   set_b_Tb_array(const double * const ext_b,
                          const double * const ext_Tb,
                          const int l_tot);
    // Reads ionization potentials
    bool   read(const char file[]);

    // Calculates Pm(b)
    void   setup();

    // Writes probabilities
    void   write(const char file[]) const;
    
    // maximal m value
    int    m_fold_max() const {return m_fold_max_;}

    // m-th Ionization potential, m starts from 0.
    double Im(const int m) const;

    // Pm(b) value
    int    b_points() const {return l_tot_;}
    double P(const int m_fold, const int ib) const;
    double b(const int ib) const;
    // Pm(b) * b value
    double Pm_x_b(const int m_fold, const int ib) const;
    
    double P_NmEta(const int N, const int m, const int m_Max) const; // eq(47)

private:
                                     // Comp. Phys. Comm. 184 (2013) 432
    const double OVERFLOW_CONST;     // for the equation (45)
    const double ZERO_CONST;         // for the equation (46)

    const IntegralTb_TensorSum * integral_lr_Tb_;   // calculates T(b) values
    const DepositParser  * d_parser_;

    int      m_fold_max_;
    double * Im_, * eta_;        // equations (23)-(25)
    double * Pm_;                // saves Pm(b) here
    int      l_tot_;             // number of the b-grid points
    const double * b_;
    const double * Tb_;          // saves T(b) values here

    // Im, eta arrays deallocation
    void  deallocate_Im_eta_();

    // Im, eta arrays allocation
    void  allocate_Im_eta_(const int m_size);

    // All elements of the array Pm are set in zero
    // for every b_l and m.
    void  set_in_zero_P_(const int mm);

    // Calculates Pm(b)
    void  calc_P_(const int m);

    // Equation (25)
    int   setup_all_eta_array_(const int i_b);

    // equation (47)
    double Si_to_Sm_(const int N, const int m, const int i) const;
};

////////////////////////////////////////////////////////////////////////////

 #endif // _PROBABLITY_M_FOLD_H_

// end of file


