#include <iostream>
#include <cmath>
#include <ctime>
#include <iomanip>
#include <cstdlib>
#include "integral_tensor_sum.h"
#include "energy_gain.h"
#include "ccross_2d.h"
#include "rho_gauss.h"
#include "deposit_parser.h"
#include "quad_weights.h"

using std::cout;
using std::endl;

///////////////////////////////////////////////////////////////////////////////

IntegralTb_TensorSum::IntegralTb_TensorSum():
DltExy_(new EnergyGain),
cross2d_(new Cross2D),
gauss_rho_(new RhoGaussLowRank),
//weights_2x_(new TrapezoidWeights),
//weights_3y_(new TrapezoidWeights),
weights_2x_(new SimpsonWeights),
weights_3y_(new SimpsonWeights),
sum_Uexph_(0) {

}

IntegralTb_TensorSum::~IntegralTb_TensorSum() {

    delete DltExy_;
    delete cross2d_;
    delete gauss_rho_;
    delete weights_2x_;
    delete weights_3y_;

    delete[] sum_Uexph_;
}

void IntegralTb_TensorSum::setup(const DepositParser * const dpstParser) {

    if (!dpstParser) {
        cout << "T(b) Error: zero pointer in parser setup." << endl;
        exit(0);
    }

    gauss_rho_->init(dpstParser);

    DltExy_->setup(dpstParser);
    DltExy_->print_initial_info();

    cross2d_->setup_maxvol_tolerance(dpstParser->maxvol_tolerance());
    cross2d_->set_rank_trancation(dpstParser->cross_error());
    cross2d_->setup_function(DltExy_);
}

void IntegralTb_TensorSum::setup_rho_USV(const int k_shell) {

    if (!DltExy_->is_parser()) {
        cout << "T(b): Error setup w eta! zero pointer (parser)" << endl;
        exit(0);
    }

    DltExy_->fix_shell_index(k_shell);

    const double beta = DltExy_->parser().C3(k_shell);
    const int C2_power = static_cast<int>(2 * DltExy_->parser().C2(k_shell)) - 2;

    gauss_rho_->set_ipower_of_r(C2_power);
    gauss_rho_->set_dpower_of_exp2(beta);
    gauss_rho_->calculate_w_ab();
    check_rho(k_shell);

    clock_t time_0 = clock();
    cross2d_->find_approximate_decomposition();
    clock_t time_1 = clock();

    const double d1 = time_1 - time_0;
    cout << "   Cross time = " << d1/CLOCKS_PER_SEC << " Sec.   ";
    cout << " Rank = " << cross2d_->rank() << endl;

    // Setup quadrature weights
    weights_2x_->set_size(DltExy_->parser().Nx() * 2);
    weights_2x_->setup_weights();
    weights_3y_->set_size(DltExy_->parser().Ny() * 3);
    weights_3y_->setup_weights();

    // Hash integrals for x mesh
    init_sum_Uexph_();
}

bool IntegralTb_TensorSum::check_rho(const int k_shell) {

    if (!(DltExy_->is_parser())) {
        cout << "Integral Tensor Sum: Check rho Error!" << endl;
        cout << "Zero parser pointer." << endl;
        return false;
    }

    const double r_max = std::max<double>(DltExy_->bx_right_bound(),
                                          DltExy_->by_right_bound());
    int Np1 = std::max(DltExy_->parser().Nx(), DltExy_->parser().Ny());
    if (Np1 % 2) {
        ++Np1;
    }
    const int Np = Np1;
    const double hr = r_max / Np;

    // Error in C0-norm
    double local_err = fabs(gauss_rho_->calc_rho_sum(r_max) - gauss_rho_->slater_nn_rho(r_max));
    double local_r0  = r_max;

    double sum = 0;
    const int Np_over_2 = static_cast<int>(0.5 * Np);
    for (int k = 0; k < Np_over_2; ++k) {

        // Simpson integration
        const double x_0 = 0 + hr * (2 * k);
        const double x_1 = 0 + hr * (2 * k + 1);
        const double x_2 = 0 + hr * (2 * k + 2);

        const double df_0 = gauss_rho_->calc_rho_sum(x_0) - gauss_rho_->slater_nn_rho(x_0);
        const double df_1 = gauss_rho_->calc_rho_sum(x_1) - gauss_rho_->slater_nn_rho(x_1);
        const double df_2 = gauss_rho_->calc_rho_sum(x_2) - gauss_rho_->slater_nn_rho(x_2);

        sum += (df_0 * df_0 + 4.0 * df_1 * df_1 + df_2 * df_2);

        // local error search
        if (local_err < fabs(df_0)) {
            local_err = fabs(df_0);
            local_r0  = x_0;
        }
        if (local_err < fabs(df_1)) {
            local_err = fabs(df_1);
            local_r0  = x_1;
        }
    }
    sum *= hr / 3.0;
    sum = sqrt(sum);

    cout << "   Rho error:  integral  = ";
    cout << std::scientific << sum << endl;
    cout << "               local max = " << local_err;
    cout << "  in point r0 = " << std::fixed << local_r0 << endl;

    return true;
}

int IntegralTb_TensorSum::shell_index() const {
    return DltExy_->k_shell();
}

int IntegralTb_TensorSum::b_points() const {
    return DltExy_->parser().Ny() + 1;
}

double IntegralTb_TensorSum::b(const int l) const {
    return DltExy_->parser().hy() * l;
}

double IntegralTb_TensorSum::value(const int l) const {

    const double hy = DltExy_->parser().hy();

    const int Nb = DltExy_->parser().Ny();    // shift paramter
    const int size_gauss = gauss_rho_->size();

    double sum_tot = 0;
    for (int alpha = 0; alpha < gauss_rho_->size(); ++alpha) {

        const double eta_a = gauss_rho_->eta(alpha);
        const double w_a   = gauss_rho_->w(alpha);

        const int jj0 = gauss_rho_->y_start_index(alpha);
        const int jj1 = gauss_rho_->y_stop_index(alpha);

        double sum_cross = 0;
        for (int r = 0; r < cross2d_->rank(); ++r) {

            // 1. x-mesh sum is already done (hashed)
            double sum_xi = sum_Uexph_[alpha + r * size_gauss];

            // 2. y-mesh sum
            double sum_yj = 0;
            for (int j = jj0; j <= jj1; ++j) {
                const double yj = DltExy_->parser().ax_left_bound() + hy * j;
                sum_yj += cross2d_->Vt(r, j + Nb - l) * exp(-eta_a * yj * yj) * weights_3y_->w(j);
            }
            sum_yj *= hy;
            const double sigma_r = cross2d_->Sigma(r);
            sum_cross += sigma_r * sum_xi * sum_yj;
        }
        const double w_to_eta = w_a/sqrt(eta_a);
        sum_tot += sum_cross * w_to_eta;
    }

    const int shell_k = DltExy_->k_shell();
    const double C1 = DltExy_->parser().C1(shell_k);
    const double pi_dot_4 = 4.0 * M_PI;

    sum_tot *= sqrt(M_PI) * C1 * C1 * DltExy_->parser().Nel(shell_k) / pi_dot_4;

    return sum_tot;
}

void IntegralTb_TensorSum::init_sum_Uexph_() {

    const int size_gauss = gauss_rho_->size();
    const int cross_rank  = cross2d_->rank();

    delete[] sum_Uexph_;
    sum_Uexph_ = new double[size_gauss * cross_rank];

    const double hx = DltExy_->parser().hx();

    for (int alpha = 0; alpha < size_gauss; ++alpha) {

        const double eta_a = gauss_rho_->eta(alpha);
        const double w_a   = gauss_rho_->w(alpha);

        const int ii0 = gauss_rho_->x_start_index(alpha);
        const int ii1 = gauss_rho_->x_stop_index(alpha);

        for (int r = 0; r < cross_rank; ++r) {

            double sum_xi = 0;
            for (int i = ii0; i <= ii1; ++i) {
                const double xi = DltExy_->ax_left_bound() + hx * i;
                sum_xi += cross2d_->U(i,r) * exp(-eta_a * xi * xi) * weights_2x_->w(i);
            }
            sum_xi *= hx;

            sum_Uexph_[alpha + r * size_gauss] = sum_xi;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////

// End of the file

