#ifndef _INTEGRAL_TENSOR_SUM_H_
#define _INTEGRAL_TENSOR_SUM_H_

///////////////////////////////////////////////////////////////////////////////

class Cross2D;
class EnergyGain;
class DepositParser;
class RhoGaussLowRank;
class QuadWeights;


/*! In this class an integration of the energy gain and
 *  electron density product is done by means of the low rank
 *  decomposition approach. */

class IntegralTb_TensorSum {
public:
    IntegralTb_TensorSum();
    ~IntegralTb_TensorSum();

    // Input data
    void   setup(const DepositParser * const dpstParser);

    // Calculates density expansion, cross and U-hash for given shell
    void   setup_rho_USV(const int k_shell);

    // Compare Slater and Gauss sum density
    bool   check_rho(const int k_shell);

    // Returns index of the current shell
    int    shell_index() const;

    int    b_points() const;
    double b(const int l) const;
    double value(const int l) const;

private:

    EnergyGain      * const  DltExy_;
    Cross2D         * const  cross2d_;
    RhoGaussLowRank * const  gauss_rho_;
    QuadWeights     * const  weights_2x_;
    QuadWeights     * const  weights_3y_;
    double          *        sum_Uexph_;

    void  init_sum_Uexph_();
};

///////////////////////////////////////////////////////////////////////////////

#endif  // _INTEGRAL_TENSOR_SUM_H_

// End of the file

