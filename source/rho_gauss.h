#ifndef _RHO_GAUSS_H_
#define _RHO_GAUSS_H_

///////////////////////////////////////////////////////////////////////////////

class RhoGaussLowRank;
class DepositParser;
class QuadWeights;

// pointer to member function
typedef double (RhoGaussLowRank::*etfXet_p)(const double, const double) const;


/*! class Expands Slater density N_electrons * Pnl(r) * Pnl(r)
 *  Pnl(r) = C1 * r^mu * exp(-beta * r)
 *  as a sum of Gaussians:
 *  sum_k( w_k * exp(- eta_k * r^2) )
 */
class RhoGaussLowRank {
public:
    RhoGaussLowRank();
    ~RhoGaussLowRank();

    void  init(const DepositParser * const parser_d);

    void  set_ipower_of_r(const int a);         // r^a, integer power
    void  set_dpower_of_exp2(const double b);   // exp(-2 b), double power
    void  calculate_w_ab();                     // calculates coefficients

    /* Calculates directly sum at given point r */
    double calc_rho_sum(const double r);
    double slater_nn_rho(const double r);  // not normilized

    int     size() const;   // size of w_ab array with fixed a and b;
    double  w(const int alpha) const;
    double  eta(const int alpha) const;

    // For fixed eta_k in 1D Integral summarize from x_start to x_end
    int  x_start_index(const int alpha) const {return x_start_index_[alpha];}
    int  x_stop_index (const int alpha) const {return x_end_index_[alpha];}
    // y-array is not shifted instead of as z-tilde
    int  y_start_index(const int alpha) const {return y_start_index_[alpha];}
    int  y_stop_index (const int alpha) const {return y_end_index_[alpha];}

private:
    const DepositParser * d_parser_;   // parser with input data
    QuadWeights * const weights_s_;

    const int a_max_;     // a_max = 10; (integer power of r)

    int    a_ipower_;     // r^a
    double b_dpower_;     // exp(-2 b)

    double t_minus_inf_;  // t in [t_minus_inf_, t_plus_inf_]
    double t_plus_inf_;
    int    Nr_;     // number of grid points for t
    double ht_;     // t-step = (t_plus_inf_ - t_minus_inf_)/Nr;

    const double exp_max_limit_;  // 650  e(-650) = 5.1e-283, if > then == 0
    const double d_sqrt_pi_;

    double * w_;
    double * eta_;            // eta is the same for all a and b.
    double * tk_;

    int * x_start_index_;   // from the bisection search
    int * x_end_index_;
    int * y_start_index_;
    int * y_end_index_;

    etfXet_p fX_pntr_;             // pointer to power-function for the current a.
    int      trancated_size_;      // size of w and eta after trancation
    const double ZERO_LIMIT_gx_;

    bool init_flag_;               // this class does not work without init()

    void   allocate_memory_(const int Nsz);  // all arrays new[]    ...
    void   deallocate_memory_();             // all arrays delete[] ...

    void   setup_tk_mesh_();
    void   setup_eta_();
    void   reset_pointer_();      // resets pointer to function
    void   setup_whole_w_();
    void   trancate_w_eta_();
    int    calc_Gaussian_istart_(const double axy, const double bxy, const int Nxy,
                                 const double hxy, int * const xy_start);
    int    calc_Gaussian_istop_ (const double axy, const double bxy, const int Nxy,
                                 const double hxy, int * const xy_stop);

    double dummyX_(const double, const double) const;      // return 0
    double et_f0_et_(const double b, const double t) const;
    double et_f1_et_(const double b, const double t) const;
    double et_f2_et_(const double b, const double t) const;
    double et_f3_et_(const double b, const double t) const;
    double et_f4_et_(const double b, const double t) const;
    double et_f5_et_(const double b, const double t) const;
    double et_f6_et_(const double b, const double t) const;

    double g0_(const double x) const;
    double g1_(const double x) const;
    double g2_(const double x) const;
    double g3_(const double x) const;
    double g4_(const double x) const;
    double g5_(const double x) const;
    double g6_(const double x) const;

    double f0_(const double b, const double x) const;
    double f1_(const double b, const double x) const;
    double f2_(const double b, const double x) const;
    double f3_(const double b, const double x) const;
    double f4_(const double b, const double x) const;
    double f5_(const double b, const double x) const;
    double f6_(const double b, const double x) const;
};

///////////////////////////////////////////////////////////////////////////////

#endif // _RHO_GAUSS_H_

// End of the file
