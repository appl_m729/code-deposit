#include <iostream>
#include <cmath>
#include "energy_gain.h"
#include "deposit_parser.h"
#include "specialfunctions.h"

using std::cout;
using std::endl;

///////////////////////////////////////////////////////////////////////////////

EnergyGain::EnergyGain():
FunctionGenerator(),
deposit_parser_(0),
k_shell_(0), b_(0) {
}

EnergyGain::~EnergyGain() {
    deposit_parser_ = 0;
}

//void EnergyGain::setup(const char * filename) {
//    deposit_parser_->setup(filename);
//}

void EnergyGain::setup(const DepositParser * const Inp_Deposit_Parser) {

    deposit_parser_ = Inp_Deposit_Parser;

    set_n_rows(deposit_parser_->Nx_points());      // 2 Nx + 1
    set_m_cols(deposit_parser_->Nzeta_points());   // 3 Nz + 1

    set_ax_left_bound(deposit_parser_->ax_left_bound());       //  -ax
    set_bx_right_bound(deposit_parser_->bx_right_bound());     //   ax
    set_ay_left_bound(deposit_parser_->azeta_left_bound());    // -2ay
    set_by_right_bound(deposit_parser_->bzeta_right_bound());  //   ay
}

void EnergyGain::fix_shell_index(const int inK_shell) {

    if (inK_shell < 0 || inK_shell >= deposit_parser_->N_shells()) {
        cout << "EnergyGain: Error! can not fix shell = " << inK_shell << endl;
        cout << "Shells number = " << deposit_parser_->N_shells() << endl;
        exit(0);
    }
    k_shell_ = inK_shell;
}

void EnergyGain::fix_parameter_b(const double b_in) {

    if (b_in < 0.0 || b_in > 1024) {
        cout << "EnergyGain: Error! can not fix b = " << b_in << endl;
        exit(0);
    }
    b_ = b_in;
}

//virtual
double EnergyGain::value(const int i, const int j) const {

    // Grid parameters
    const double Dx_ab = bx_right_bound() - ax_left_bound();
    const double Dy_ab = by_right_bound() - ay_left_bound();
    const double i_over_n = static_cast<double>(i) / static_cast<double>(n_rows() - 1);
    const double j_over_m = static_cast<double>(j) / static_cast<double>(m_cols() - 1);
    const double xi = ax_left_bound() + Dx_ab * i_over_n;
    const double yj = ay_left_bound() + Dy_ab * j_over_m;

    // Deposit parameters
    const double b = 0;      // expanded mesh  z_tilde = y - b
    const double p = sqrt(xi * xi + (b - yj) * (b - yj));

    const double Vi = deposit_parser_->Vi();
    const double Za = deposit_parser_->Za();
    //const double Ra = deposit_parser_->Ra();

    const double A1 = deposit_parser_->A_exp(0);
    const double A2 = deposit_parser_->A_exp(1);
    const double A3 = deposit_parser_->A_exp(2);

    const double alf_1 = deposit_parser_->alpfa_exp(0);
    const double alf_2 = deposit_parser_->alpfa_exp(1);
    const double alf_3 = deposit_parser_->alpfa_exp(2);

    const double AFsum = A1 * yK1_(alf_1 * p) +
                         A2 * yK1_(alf_2 * p) +
                         A3 * yK1_(alf_3 * p);
    // E_more
    const double znam1m = Vi * p;
    const double znam2m = Za / Vi;
    const double znam_more = znam1m * znam1m + znam2m * znam2m;
    const double zs = Za *AFsum;
    const double dE_more = 2.0 * zs * zs / znam_more;
    // E_less
    const double ug   = deposit_parser_->u(k_shell_);
    const double Neff = deposit_parser_->Zeff(k_shell_);
    const double vr = sqrt(Vi * Vi + ug * ug);
    const double znam_less = vr * p + 4.0 * ug / (Vi * Vi);
    double dE_less = 2.0 * Neff * ug * AFsum / znam_less;

    return dE_less * SmearStep_(ug - Vi) +
           dE_more * SmearStep_(Vi - ug);
}

double EnergyGain::yK1_(const double xx) const {

    double xt = xx;
    if (fabs(xt) <= 1e-10) {xt = 1e-10;}

    return xt*alglib::besselk1(xt);
}

double EnergyGain::SmearStep_(const double xx) const {

    const double ksmear = deposit_parser_->k_smear();
    return 1.0/(exp(-ksmear*xx) + 1.0);
}

///////////////////////////////////////////////////////////////////////////////


// End of the file

