#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include "deposit_solver_lr.h"
#include "deposit_parser.h"
#include "integral_tensor_sum.h"
#include "probability_m.h"
//#include "interpolation.h"

using std::cout;
using std::endl;
using std::setw;
using std::setprecision;
using std::ofstream;

///////////////////////////////////////////////////////////////////////////////

const double aBOHR2(0.280028413 * 1e-16);

DepositSolverLR::DepositSolverLR():
d_parser_(new DepositParser()),
integral_lr_Tb_(new IntegralTb_TensorSum()),
Pm_fold_(new Probability_mfold),
MAX_ITER_(128),
Nb_(0),
l_tot_(0),
b_tot_(0),
Tb_(0),
bl_(0),
Tbk_(0),
sig_tot_(0),
tt_start_(time(0)) {
    cout << endl << " ****  CODE DEPOSIT 2014 (VERSION 2.0/S) ****  " << endl << endl;
}

DepositSolverLR::~DepositSolverLR() {

    delete d_parser_;
    delete integral_lr_Tb_;
    delete Pm_fold_;

    free_Tb_arrays_();
}

void DepositSolverLR::solve(const char inp_file[], const char out_file[]) {

    // setup
    d_parser_->setup(inp_file);
    integral_lr_Tb_->setup(d_parser_);

    // b-mesh setup
    Nb_ = integral_lr_Tb_->b_points();
    free_Tb_arrays_();
    allocate_Tb_arrays_();

    // Tb save
    calc_energy_Tb_();
    write_Tb_(out_file);

    // Sigma tot from b_max. b_max is calculated here.
    // b_max is placed in the bl_ array in [Nb_] position
    // Tb(b_max) is placed as well.
    calc_Sigma_tot_();

    // Calc Sigma m-fold
    Pm_fold_->set_parser(d_parser_);
    Pm_fold_->set_integral_Tb(integral_lr_Tb_);
    Pm_fold_->set_b_Tb_array(bl_, Tb_, l_tot_);

    if (Pm_fold_->read(inp_file)) {

        Pm_fold_->setup();
        calc_Sigma_m_fold_();
        write_Pm_(out_file);
        check_Pm_();
    }
    measure_time_();
}

void DepositSolverLR::write(const char out_file[]) const {


}

void DepositSolverLR::free_Tb_arrays_() {
    delete[] bl_;  bl_  = 0;
    delete[] Tb_;  Tb_  = 0;
    delete[] Tbk_; Tbk_ = 0;
}

void DepositSolverLR::allocate_Tb_arrays_() {

    bl_  = new double[ Nb_ + 3];  // [0, Nb] + 2 additional points
    Tb_  = new double[ Nb_ + 3];
    Tbk_ = new double[(Nb_ + 3) * d_parser_->N_shells()];
}

void DepositSolverLR::calc_energy_Tb_() {

    cout << endl << " Start calculation of Tb procedure:" << endl;

    const int Nshells = d_parser_->N_shells();

    for (int k_shell = 0; k_shell < d_parser_->N_shells(); ++k_shell) {

            cout << endl << " Shell(" << k_shell << "):";
            cout << setprecision(1) << setw(6)  << d_parser_->Nel(k_shell);
            cout << setprecision(3) << setw(12) << d_parser_->C1(k_shell);
            cout << setprecision(1) << setw(6)  << d_parser_->C2(k_shell);
            cout << setprecision(3) << setw(9)  << d_parser_->C3(k_shell) << endl;

            integral_lr_Tb_->setup_rho_USV(k_shell);
            calc_Tb_data_to_Tb_array_();              // for currently fixed shell
    }

    // Calc Tb_total as sum of Tb_k at fixed b[l]
    for (int l = 0; l < Nb_; ++l) {

        bl_[l] = integral_lr_Tb_->b(l);
        double sum_Tbl = 0.0;
        for (int k_shell = 0; k_shell < Nshells; ++k_shell) {
            sum_Tbl += Tbk_[k_shell * Nb_ + l];
        }
        Tb_[l] = sum_Tbl;

//        if (Tb_[l] > 1e-6) {
//            b_Tb_out_(bl_[l], Tb_[l]);
//        }
    }
}

void DepositSolverLR::calc_Tb_data_to_Tb_array_() {

    cout << "   Low rank integration (";
    cout << integral_lr_Tb_->b_points() << " integrals):     ";

    const double Nbp = static_cast<double>(integral_lr_Tb_->b_points());
    const int step_rate = static_cast<int>(Nbp / 100.0) + 1;

    print_percent_(0);
    clock_t start_tm = clock();       // Start measure time
    const int k_shell = integral_lr_Tb_->shell_index();
    for (int l = 0; l < Nb_; ++l) {

        // --- Dynamic printing ---
        if (!((l + 1) % step_rate)) {
            const double rate_nominator = static_cast<double>(l + 1);
            const int percent_rate = static_cast<int>(rate_nominator / Nbp * 100.0);
            print_percent_(percent_rate);
        }
        if ((l + 1) == integral_lr_Tb_->b_points()) {
            print_percent_(100);
        }
        // --- End dynamic printing ---

        // Calculate and save value in the b-mesh point
        Tbk_[k_shell * Nb_ + l] = integral_lr_Tb_->value(l);
    }
    clock_t finish_tm = clock();     // Finish measure time

    const double dT_tot = fabs(finish_tm - start_tm);
    const double dT_tot_sec = dT_tot/CLOCKS_PER_SEC;
    const double dT1 = dT_tot_sec / integral_lr_Tb_->b_points();

    cout << endl;
    cout << "   Total time = " << std::fixed << dT_tot_sec << " Sec.";
    cout << "   Per integral = " << std::scientific << dT1 << " Sec." << std::fixed << endl;
}

void DepositSolverLR::measure_time_() {

    // mesure time
    time_t tt_stop(time(NULL));
    tm * pBuffTime = 0;
    cout << endl;
    pBuffTime = localtime(&tt_start_);
    cout << " Start time: " << asctime(pBuffTime);
    pBuffTime = localtime(&tt_stop);
    cout << " Stop  time: " << asctime(pBuffTime);
    cout << endl;
}

// formatted print_out
void DepositSolverLR::b_Tb_out_(const double b1, const double Tb1) const {

    cout << std::fixed << setprecision(6) << setw(10) << b1;
    cout << std::fixed << setprecision(6) << setw(14) << Tb1;
    cout << endl;
}

/*! In two steps. Bisection on the b-mesh. Spline
 *  interpolation and smooth bisection then. */
void DepositSolverLR::calc_Sigma_tot_() {

    const double I1_sig = d_parser_->I1_first_au();
    int lb_left  = 0;
    int lb_right = Nb_ - 1;
    double TbI1_left  = Tb_[lb_left]  - I1_sig;
    double TbI1_right = Tb_[lb_right] - I1_sig;  // initial contracting range

    cout << endl << " Total cross-section calculation:" << endl;
    cout << " I1 = ";
    cout << std::fixed << setprecision(4) << setw(10) << I1_sig << " a.u." << endl;
    cout << " b_min = "   << std::fixed << setprecision(4) << setw(7) << bl_[lb_left ];
    cout << "   b_max = " << std::fixed << setprecision(4) << setw(7) << bl_[lb_right];
    cout << endl;
    cout << endl << " Discrete bisection search:" << endl;
    cout << "      b       T(b) - I1" << endl;

    b_Tb_out_(bl_[lb_left],  TbI1_left);
    b_Tb_out_(bl_[lb_right], TbI1_right);

    // unphysical values
    if ( (TbI1_left < 1e-12) || (TbI1_right > 1e-12) ) {

        b_tot_   = 0.0;
        sig_tot_ = 0.0;

        cout << " Sigma_total = 0" << endl;
        return;
    }

    Tb_bisection_on_mesh_(lb_left, lb_right);         // lb_left lb_right by reference
    b_tot_ = Tb_spline_bisection_(lb_left, lb_right);

    cout << endl << " b_total     = ";
    cout << std::fixed << setprecision(6) << setw(11) << b_tot_ << endl;
    sig_tot_ = M_PI * b_tot_ * b_tot_;
    cout << " Sigma_total = ";
    cout << std::fixed << setprecision(6) << setw(11) << sig_tot_ << " a.u." << endl;
    cout << " Sigma_total = ";
    const double sig_tot_sm(M_PI * b_tot_ * b_tot_ * aBOHR2);
    cout << std::scientific << setprecision(6) << setw(15) << sig_tot_sm;
    cout << " cm2" << endl;
}

void DepositSolverLR::Tb_bisection_on_mesh_(int & lb_left, int & lb_right) {

    // starts bisection calculations on mesh
    const double I1_sig = d_parser_->I1_first_au();

    int iter = 0;
    while ((++iter < MAX_ITER_) && (lb_left < lb_right)) {

        const double lb_mid = 0.5 * (lb_left + lb_right);
        const int lb_mid_floor = static_cast<int>(floor(lb_mid));  // left  l
        const int lb_mid_ceil  = static_cast<int>( ceil(lb_mid));  // right l

        const double TbI1_mid_floor = Tb_[lb_mid_floor] - I1_sig;  // T left
        const double TbI1_mid_ceil  = Tb_[lb_mid_ceil]  - I1_sig;  // T right

        if (TbI1_mid_ceil < 0.0) {

            // the middle point is taken as a right-hand side point
            lb_right = lb_mid_ceil;
            const double TbI1_right = TbI1_mid_ceil;
            b_Tb_out_(bl_[lb_right], TbI1_right);

        } else {
            // the middle point is taken as a left-hand side point
            lb_left = lb_mid_floor;
            const double TbI1_left = TbI1_mid_floor;
            b_Tb_out_(bl_[lb_left], TbI1_left);
        }

        if ((lb_left + 1) == lb_right) break;
    }  // end while loop

    if (MAX_ITER_ == iter) {
        cout << endl << " Tb bisection error! Number of iterations = MAX_ITER = ";
        cout << MAX_ITER_ << endl << endl;
        exit(0);
    }
}

/*! Create spline interpolant needed for spline bisection search. */
void DepositSolverLR::create_spline_interpolant_(const int lb_left, const int lb_right,
                                                 spline1dinterpolant & s1i) {

    const int n_half_range = 4; // nodes for spline: 2 * (n_half_range + 1)
    const int n_full_range = 2 * (n_half_range + 1);

    // Check if l is in proper range.
    if (lb_left < n_half_range) {
        cout << "Tb spline bisection (left)  Error!" << endl;
        exit(0);
    }
    if (lb_right + 1 + n_half_range > Nb_) {
        cout << "Tb spline bisection (right) Error!" << endl;
        exit(0);
    }

    // setup spline data
    real_1d_array bl_array, Tb_array;
    bl_array.setlength(n_full_range);
    Tb_array.setlength(n_full_range);

    for (int k = lb_left - n_half_range, i = 0; k <= (lb_right + n_half_range); ++k, ++i) {
        bl_array[i] = bl_[k];
        Tb_array[i] = Tb_[k];
    }

    ae_int_t nb_type = 2;  // natural_bound
    spline1dbuildcubic(bl_array, Tb_array, n_full_range, nb_type, 0, nb_type, 0, s1i);
}

double DepositSolverLR::Tb_spline_bisection_(const int lb_left, const int lb_right) {

    cout << endl << " Spline bisection search:" << endl;
    cout << "      b       T(b) - I1" << endl;

    spline1dinterpolant s1i;
    create_spline_interpolant_(lb_left, lb_right, s1i);

    const double I1_sig = d_parser_->I1_first_au();
    const double EPS_ERR(1e-3);

    double x1,x2, f1,f2;          // contracting range

    // setup initial range
    x1 = bl_[lb_left ];
    x2 = bl_[lb_right];

    f1 = spline1dcalc(s1i, x1) - I1_sig;
    f2 = spline1dcalc(s1i, x2) - I1_sig;

    // starts smooth bisection calculations
    int iter = 0;
    while ( (++iter < MAX_ITER_) && (fabs(x2 - x1) > EPS_ERR) ) {

        const double t_xv = 0.5 * (x1 + x2);
        const double t_fv = spline1dcalc(s1i, t_xv) - I1_sig;

        if ( t_fv < 0) {
            // the middle point is taken as a right-hand side point
            x2 = t_xv;
            f2 = t_fv;
        } else {
            // the middle point is taken as a left-hand side point
            x1 = t_xv;
            f1 = t_fv;
        }
        b_Tb_out_(t_xv, t_fv);
    }

    if (MAX_ITER_ == iter) {
        cout << endl << " Error! Number of iterations spline search = MAX_ITER = ";
        cout << MAX_ITER_ << endl << endl;
        exit(0);
    }

    // Get b_max from parabola extrapolation
    const double max_b_extr = b_tot_extrapolate_(x1, x2, s1i);

    // Save (b_max, Tb(b_max)) and makes even number of points
    finish_b_Tb_right_bound_(lb_left, s1i, max_b_extr);

    return max_b_extr;
}

// lb_left - last index of the arrays b[], Tb[].
//
// If number of mesh points is EVEN, then 2 points are added after:
// (b[lb_left] + b_max)/2 in [lb_left + 1] and b_max in [lb_left + 2]
// (the Simpson rule is used then, so two points
//  with an equivalent steps are needed)
//
// If number of mesh points is ODD, then b_max is added in [lb_left + 1] and
// (b[lb_left] + b_max)/2 replaces the [lb_left]

void DepositSolverLR::finish_b_Tb_right_bound_(const int lb_left,
                                               spline1dinterpolant & s1i,
                                               const double max_b_extr) {
    if ( !(lb_left % 2) ) {

        // EVEN number of points. Add two point.
        const double bb_left = bl_[lb_left];
        const double bb_midl = 0.5 * (bb_left + max_b_extr);

        bl_[lb_left + 1] = bb_midl;
        bl_[lb_left + 2] = max_b_extr;

        l_tot_ = lb_left + 2;

    } else {

        // ODD number of points
        const double bb_left = bl_[lb_left - 1];
        const double bb_midl = 0.5 * (bb_left + max_b_extr);

        bl_[lb_left]     = bb_midl;
        bl_[lb_left + 1] = max_b_extr;

        l_tot_ = lb_left + 1;
    }

    // Calculate Tb[l_tot_], Tb[l_tot - 1]
    Tb_[l_tot_ - 1] = spline1dcalc(s1i, bl_[l_tot_ - 1]);
    Tb_[l_tot_] = spline1dcalc(s1i, bl_[l_tot_]);
}

double DepositSolverLR::b_tot_extrapolate_(const double x_left, const double x_right,
                                           spline1dinterpolant & s1i) const {

    cout << endl << " Interpolate:  " << endl;
    cout << "      b       T(b) - I1" << endl;

    const double dxx = x_right - x_left;               //  interval

    const double t_middle = 0.5 * (x_left + x_right);  //  middle point
    const double t_left   = t_middle - 0.25 * dxx;
    const double t_right  = t_middle + 0.25 * dxx;

    const double I1_sig = d_parser_->I1_first_au();

    // left
    const double f_left = spline1dcalc(s1i, t_left) - I1_sig;
    b_Tb_out_(t_left, f_left);

    // middle
    const double f_middle = spline1dcalc(s1i, t_middle) - I1_sig;
    b_Tb_out_(t_middle, f_middle);

    // right
    const double f_right = spline1dcalc(s1i, t_right) - I1_sig;
    b_Tb_out_(t_right, f_right);

    return parabola_extrapolate_(t_left, f_left * f_left,
                                 t_middle, f_middle * f_middle,
                                 t_right, f_right * f_right);
}

double DepositSolverLR::parabola_extrapolate_(const double x1, const double y1,
                                              const double x2, const double y2,
                                              const double x3, const double y3) const {
    const double v1 = x1 * (y2 - y3);
    const double v2 = x2 * (y3 - y1);
    const double v3 = x3 * (y1 - y2);

    return 0.5 * (x1 * v1 + x2 * v2 + x3 * v3)/(v1 + v2 + v3);
}

// Calculates Sigma m-fold by using Pm(b)
void DepositSolverLR::calc_Sigma_m_fold_() {

    cout << endl << " m-fold cross-sections calculation:" << endl;

    // prints out input ionization potentials
    char str1[256];
    for (int m = 0; m < Pm_fold_->m_fold_max(); ++m) {

        sprintf(str1,"I_%d",m+1);
        cout << setw(5) << str1 << " = ";
        cout << std::fixed << setprecision(4) << setw(10) << Pm_fold_->Im(m);
        cout << " a.u.";// << endl;

        cout << " = ";
        cout << std::fixed << setprecision(3) << setw(12);
        cout << Pm_fold_->Im(m) * 2.0 * d_parser_->Ebohr();
        cout << " eV" << endl;
    }

    cout << endl << "  m-fold Cross-sections:" << endl;
    cout << "    m       a.u.        cm2" << endl;

    double sum_Sigma_m = 0;
    for (int m = 0; m < Pm_fold_->m_fold_max(); ++m) {

        // calculates m-fold cross-section for given m
        const double t_sigma_m = integrate_sigma_mfold_(m);
        sum_Sigma_m += t_sigma_m;

        // prints out m-fold sigma for given m
        sprintf(str1,"%d",m+1);
        cout << setw(5) << str1;
        cout << std::fixed << setprecision(6) << setw(12) << t_sigma_m;
        const double t_sgm_m_cm2 = t_sigma_m * aBOHR2;
        cout << std::scientific << setprecision(6) << setw(15) << t_sgm_m_cm2 << endl;
    }

    // prints out the sum of the m-fold cross-sections
    cout << "  sum";
    cout << std::fixed << setprecision(6) << setw(12) << sum_Sigma_m;
    const double Sum_sgm_m_cm2 = sum_Sigma_m * aBOHR2;
    cout << std::scientific << setprecision(6) << setw(15) << Sum_sgm_m_cm2 << endl;
}

double DepositSolverLR::integrate_sigma_mfold_(const int m) const {

    if (l_tot_ % 2) {
        cout << "Error in DepositSolverLR: Integrate Sigma_m_fold." << endl;
        cout << " l_tot = " << l_tot_ << " is odd. Simpson rule break;" << endl;
        exit(0);
    }

    const int N_over_2 = l_tot_ / 2;
    double sum = 0;
    for (int i = 0; i < N_over_2; ++i) {

        const double a = Pm_fold_->b(2 * i);
        const double b = Pm_fold_->b(2 * i + 2);

        const double fa = Pm_fold_->Pm_x_b(m, 2 * i);
        const double fm = Pm_fold_->Pm_x_b(m, 2 * i + 1);
        const double fb = Pm_fold_->Pm_x_b(m, 2 * i + 2);

        sum += (b - a) * (fa + 4.0 * fm + fb);
    }

    return 2 * M_PI * sum / 6.0;
}

void DepositSolverLR::print_percent_(const int value) const {

    if (value > 9)  {cout << "\b";}
    if (value > 99) {cout << "\b";}

    cout << "\b\b";
    cout << value << "%";
    cout.flush();
}


void DepositSolverLR::write_Tb_(const char ofile[]) const {

    // make proper filename
    char str1[256];
    sprintf(str1, "Tb_%s", ofile);
    write(str1);
    ofstream fout(str1);

    // writes caption
    fout.precision(16);
    fout << "b  Tb_tot ";
    for (int k_shell = 0; k_shell < d_parser_->N_shells(); ++k_shell) {
        fout << " Tb["<< k_shell << "] ";
    }
    fout << endl;

    // writes data
    for (int l = 0; l < Nb_; ++l) {

        fout << std::fixed << setprecision(6) << setw(10) << bl_[l];
        fout << std::fixed << setprecision(6) << setw(14) << Tb_[l];

        for (int k_shell = 0; k_shell < d_parser_->N_shells(); ++k_shell) {
            fout << std::fixed << setprecision(6) << setw(14) << Tbk_[k_shell * Nb_ + l];
        }
        fout << endl;
    }

    fout.close();
    cout << endl << " Output T(b) into the file:  " << str1 << endl;
}

void DepositSolverLR::write_Pm_(const char out_file[]) const {

    char str1[256];
    sprintf(str1,"Pm_%s", out_file);

    Pm_fold_->write(str1);
}

void DepositSolverLR::check_Pm_() {

    // Sum of Pm(b) over m for any fixed b must be equal to 1.0
    // Checks this condition

    for (int l = 0; l <= Pm_fold_->b_points(); ++l) {

        double ssum(0);
        for (int m = 0; m < Pm_fold_->m_fold_max(); ++m) {
            ssum += Pm_fold_->P(m, l);
        }

        if (fabs(ssum - 1.0) > 1e-2) {
            cout << endl << " WARNING in probability sum test:";
            cout << " in point = " << l << "   1 = " << ssum << endl;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////

// End of the file


