#include <iostream>
#include <cmath>
#include <iomanip>
#include "rho_gauss.h"
#include "deposit_parser.h"
#include "specialfunctions.h"
#include "quad_weights.h"

using std::cout;
using std::endl;

///////////////////////////////////////////////////////////////////////////////

//t_minus_inf_(-25.0), t_plus_inf_(70.0), //t_minus_inf_(-4.0), t_plus_inf_(45.0),
//Nr_(2014), //Nr_(600)

RhoGaussLowRank::RhoGaussLowRank():
d_parser_(0),
weights_s_(new TrapezoidWeights()),
a_max_(6),
a_ipower_(0),
b_dpower_(1.0),
t_minus_inf_(-3.0), t_plus_inf_(45.0),
Nr_(250), ht_((t_plus_inf_ - t_minus_inf_)/Nr_),
exp_max_limit_(650),
d_sqrt_pi_(1.0/sqrt(M_PI)),
w_(0), eta_(0),
tk_(0),
x_start_index_(0),
x_end_index_(0),
y_start_index_(0),
y_end_index_(0),
fX_pntr_(&RhoGaussLowRank::dummyX_),
trancated_size_(Nr_ + 1),
ZERO_LIMIT_gx_(2.0e-3),
init_flag_(false) {

}

RhoGaussLowRank::~RhoGaussLowRank() {
    deallocate_memory_();
    delete weights_s_;
}

void RhoGaussLowRank::init(const DepositParser * const inpParserDpst) {

    if (!inpParserDpst) return;

    d_parser_ = inpParserDpst;

    t_minus_inf_ = d_parser_->tk_minus_infty();
    t_plus_inf_  = d_parser_->tk_plus_infty();
    Nr_ = d_parser_->tk_Npoints();
    ht_ = (t_plus_inf_ - t_minus_inf_) / Nr_;

    deallocate_memory_();
    allocate_memory_(Nr_ + 1);
    trancated_size_ = Nr_ + 1;

    weights_s_->set_size(Nr_);
    weights_s_->setup_weights();

    // all setup is done properly
    init_flag_ = true;
}

void RhoGaussLowRank::set_ipower_of_r(const int a_inp) {

    if ((a_inp < 0) || (a_inp > a_max_)) {

        cout << "RhoGaussLowRank: power of r is out of range:" << endl;
        cout << "a = " << a_inp;
        cout << "  must be in [0," << a_max_ << "]" << endl;
    }
    a_ipower_ = a_inp;

    reset_pointer_();
}

void RhoGaussLowRank::set_dpower_of_exp2(const double b_inp) {

    if ((b_inp <= 1e-12)) {

        cout << "RhoGaussLowRank: power of exp2 is out of range:" << endl;
        cout << "b = " << b_inp;
        cout << "  must be > 0" << endl;
    }
    b_dpower_ = b_inp;
}

void RhoGaussLowRank::calculate_w_ab() {

    if (!init_flag_) {
        cout << "Error : Wrong init in RhoGaussLowRank." << endl;
        exit(0);
    }

    // flag
    setup_tk_mesh_();
    setup_eta_();   // eta could chaanged when truncate

    setup_whole_w_();
    trancate_w_eta_();  // w_to_sqrt_eta is calculated here

    // 2. Gaussian exp(-eta*xi^2) truncation
    const double ax = d_parser_->ax_left_bound();
    const double bx = d_parser_->bx_right_bound();
    const int    Nx = d_parser_->Nx_points();
    const double hx = d_parser_->hx();
    // x_start
    const int sum_xa = calc_Gaussian_istart_(ax, bx, Nx, hx, x_start_index_);
    // x_end
    const int sum_xb = calc_Gaussian_istop_(ax, bx, Nx, hx, x_end_index_);

    const double ay = d_parser_->ay_left_bound();
    const double by = d_parser_->by_right_bound();
    const int    Ny = d_parser_->Ny_points();
    const double hy = d_parser_->hy();
    // y_start
    const int sum_ya = calc_Gaussian_istart_(ay, by, Ny, hy, y_start_index_);
    // y_end
    const int sum_yb = calc_Gaussian_istop_(ay, by, Ny, hy, y_end_index_);

    const double dx_contraction = (sum_xb - sum_xa) / (static_cast<double>(Nx * size()));
    const double dy_contraction = (sum_yb - sum_ya) / (static_cast<double>(Ny * size()));

    cout << ";   Contraction: (" << dx_contraction << ", ";
    cout << dy_contraction << ")" << endl;
}

double RhoGaussLowRank::calc_rho_sum(const double r) {

    double sum_rho = 0.0;
    for (int k = 0; k < size(); ++k) {
        sum_rho += w_[k] * exp(-eta_[k] * r * r);
    }
    return sum_rho;
}

double RhoGaussLowRank::slater_nn_rho(const double r) {

    const double mu   = static_cast<double>(a_ipower_);
    const double beta = b_dpower_;

    return  pow(r, mu) * exp(-2.0 * beta * r);
}

int RhoGaussLowRank::size() const {
    return trancated_size_;
}

double RhoGaussLowRank::w(const int index) const {
    return w_[index];
}

double RhoGaussLowRank::eta(const int index) const {
    return eta_[index];
}

void RhoGaussLowRank::allocate_memory_(const int Nsz) {

    w_             = new double[Nsz];
    eta_           = new double[Nsz];
    tk_            = new double[Nsz];

    x_start_index_ = new int[Nsz];
    x_end_index_   = new int[Nsz];
    y_start_index_ = new int[Nsz];
    y_end_index_   = new int[Nsz];
}

void RhoGaussLowRank::deallocate_memory_() {

    delete[] w_;
    delete[] eta_;
    delete[] tk_;

    delete[] x_start_index_;
    delete[] x_end_index_;
    delete[] y_start_index_;
    delete[] y_end_index_;
}

void RhoGaussLowRank::setup_tk_mesh_() {

    for (int k = 0; k <= Nr_; ++k) {
        tk_[k] = t_minus_inf_ + ht_ * static_cast<double>(k);
    }
}

void RhoGaussLowRank::setup_eta_() {

    for (int k = 0; k <= Nr_; ++k) {
        eta_[k] = exp(tk_[k]);
    }
}

void RhoGaussLowRank::reset_pointer_() {

    switch (a_ipower_) {
    case 0:
        fX_pntr_ = &RhoGaussLowRank::et_f0_et_;
        break;
    case 1:
        fX_pntr_ = &RhoGaussLowRank::et_f1_et_;
        break;
    case 2:
        fX_pntr_ = &RhoGaussLowRank::et_f2_et_;
        break;
    case 3:
        fX_pntr_ = &RhoGaussLowRank::et_f3_et_;
        break;
    case 4:
        fX_pntr_ = &RhoGaussLowRank::et_f4_et_;
        break;
    case 5:
        fX_pntr_ = &RhoGaussLowRank::et_f5_et_;
        break;
    case 6:
        fX_pntr_ = &RhoGaussLowRank::et_f6_et_;
        break;
    default:
        cout << "Rho Gauss: Error! Dummy pointer set." << endl;
        fX_pntr_ = &RhoGaussLowRank::dummyX_;
        break;
    }
}

void RhoGaussLowRank::setup_whole_w_() {

    for (int k = 0; k <= Nr_; ++k) {
        w_[k] = weights_s_->w(k) * ht_ * (this->*fX_pntr_)(b_dpower_, tk_[k]);
    }


//    for (int k = 0; k <= Nr_; ++k) {
//        w_[k] = ht_ * (this->*fX_pntr_)(b_dpower_, tk_[k]);
//    }
//    w_[0]   *= 0.5;
//    w_[Nr_] *= 0.5;
}

void RhoGaussLowRank::trancate_w_eta_() {

    const double GAUSS_CUTOFF = d_parser_->gauss_cutoff();

    int index = 0;
    for (int k = 0; k <= Nr_; ++k) {

        const double w_to_eta = w_[k]/sqrt(eta_[k]);

        if (fabs(w_to_eta) > GAUSS_CUTOFF) {
            w_[index]   = w_[k];
            eta_[index] = eta_[k];
            ++index;
        }
    }
    trancated_size_ = index;

    cout << "   Rho size: "  << size();
    cout << " / " << (Nr_ + 1);
    cout << " = " << static_cast<double>(size()) / static_cast<double>(Nr_ + 1); // << endl;
}

/*! Takes left bound of range in x_i array from Gaussian "hat".
 *  In separated 1D integration we calculate exp(-eta_k * x_i * x_i)
 *  terms for i. For fixed eta_k we want to throw out such x_i that
 *  exp(-eta_k * x_i * x_i) is smaller then GAUSS_CUTOFF
 *  for every x_(i < i0). We search such i0 that
 *  exp(-eta_k * x_i0 * x_i0) < GAUSS_CUTOFF and
 *  exp(-eta_k * x_(i0+1) * x_(i0+1)) >= GAUSS_CUTOFF.
 */
int RhoGaussLowRank::calc_Gaussian_istart_(const double axy, const double bxy, const int Nxy,
                                           const double hxy, int * const xy_start) {

    const double GAUSS_CUTOFF = d_parser_->gauss_cutoff();

    if ((axy >= 0) || (bxy <= 0)) {
        cout << "Rho Gauss: Error in [axy,bxy] range:start!" << endl;
        cout << "axy = " << axy << "  must be < 0" << endl;
        cout << "bxy = " << bxy << "  must be > 0" << endl;
        exit(0);
    }

    int sum_start = 0;
    // find x_start
    for (int k = 0; k < size(); ++k) {

        const double eta_k = eta(k);
        int ia = 0;
        int ib = static_cast<int>(((-axy) / (bxy - axy)) * Nxy);
        int ic;

        while (ia + 1 < ib) {

            ic = (ia + ib) / 2;
            const double xi = axy + hxy * ic;
            const double eval = eta_k * xi * xi;

            if (eval >= exp_max_limit_) {
                ia = ic;
                continue;  // exponential overflow avoid
            }
            const double ve = exp(-eval);

            if (ve < GAUSS_CUTOFF) {
                ia = ic;
            }
            if (ve > GAUSS_CUTOFF) {
                ib = ic;
            }
        }
        xy_start[k] = ia;
        sum_start += ia;
//        xy_start[k] = 0;
//        sum_start += 0;
    }
    return sum_start;
}

/*! Takes right bound of range in x_i array from Gaussian "hat".
 *  In separated 1D integration we calculate exp(-eta_k * x_i * x_i)
 *  terms for i. For fixed eta_k we want to throw out such x_i that
 *  exp(-eta_k * x_i * x_i) is smaller then GAUSS_CUTOFF
 *  for every x_(i > i0). We search such i0 that
 *  exp(-eta_k * x_i0 * x_i0) >= GAUSS_CUTOFF and
 *  exp(-eta_k * x_(i0+1) * x_(i0+1)) < GAUSS_CUTOFF.
 */
int RhoGaussLowRank::calc_Gaussian_istop_(const double axy, const double bxy, const int Nxy,
                                          const double hxy, int * const xy_stop) {

    const double GAUSS_CUTOFF = d_parser_->gauss_cutoff();

    if ((axy >= 0) || (bxy <= 0)) {
        cout << "Rho Gauss: Error in [axy,bxy] range:stop!" << endl;
        cout << "axy = " << axy << "  must be < 0" << endl;
        cout << "bxy = " << bxy << "  must be > 0" << endl;
        exit(0);
    }

    double sum_stop = 0;
    // find x_stop
    for (int k = 0; k < size(); ++k) {

        const double eta_k = eta(k);
        int ia = static_cast<int>(((-axy) / (bxy - axy)) * Nxy);
        int ib = Nxy - 1;
        int ic;

        while (ia + 1 < ib) {

            ic = (ia + ib) / 2;
            const double xi = axy + hxy * ic;
            const double eval = eta_k * xi * xi;

            if (eval >= exp_max_limit_) {
                ib = ic;
                continue;
            }
            const double ve = exp(-eval);

            if (ve < GAUSS_CUTOFF) {
                ib = ic;
            }
            if (ve > GAUSS_CUTOFF) {
                ia = ic;
            }
        }
        xy_stop[k] = ib;
        sum_stop += ib;
//        xy_stop[k] = Nxy - 1;
//        sum_stop += Nxy - 1;
    }
    return sum_stop;
}

double RhoGaussLowRank::dummyX_(const double, const double) const {
    return 0;
}

double RhoGaussLowRank::et_f0_et_(const double b, const double t) const {
    const double et = exp(t);
    return et * f0_(b, et);
}

double RhoGaussLowRank::et_f1_et_(const double b, const double t) const {
    const double et = exp(t);
    return et * f1_(b, et);
}

double RhoGaussLowRank::et_f2_et_(const double b, const double t) const {
    const double et = exp(t);
    return et * f2_(b, et);
}

double RhoGaussLowRank::et_f3_et_(const double b, const double t) const {
    const double et = exp(t);
    return et * f3_(b, et);
}

double RhoGaussLowRank::et_f4_et_(const double b, const double t) const {
    const double et = exp(t);
    return et * f4_(b, et);
}

double RhoGaussLowRank::et_f5_et_(const double b, const double t) const {
    const double et = exp(t);
    return et * f5_(b, et);
}

double RhoGaussLowRank::et_f6_et_(const double b, const double t) const {
    const double et = exp(t);
    return et * f6_(b, et);
}

double RhoGaussLowRank::g0_(const double t) const {
    if (t < ZERO_LIMIT_gx_) return 0.0;
    return exp(-1.0 / t) / pow(t, 1.5);
}

double RhoGaussLowRank::g1_(const double t) const {
    if (t < ZERO_LIMIT_gx_) return 0.0;
    return -exp(-1.0 / t) / pow(t, 1.5) * (1.0 - 2.0 / t);
}

double RhoGaussLowRank::g2_(const double t) const {
    if (t < ZERO_LIMIT_gx_) return 0.0;
    return -exp(-1.0 / t) / pow(t, 2.5) * (1.0 - 2.0 / (3.0 * t));
}

double RhoGaussLowRank::g3_(const double t) const {
    if (t < ZERO_LIMIT_gx_) return 0.0;
    return exp(-1.0 / t) / pow(t, 2.5) * (1.0 + (4.0 / (3.0 * t) - 4.0) / t);
}

double RhoGaussLowRank::g4_(const double t) const {
    if (t < ZERO_LIMIT_gx_) return 0.0;
    return exp(-1.0 / t) / pow(t, 3.5) * (1.0 + (4.0 / (15.0 * t) - 4.0/3.0) / t);
}

double RhoGaussLowRank::g5_(const double t) const {
    if (t < ZERO_LIMIT_gx_) return 0.0;
    return -exp(-1.0 / t) / pow(t, 3.5) *
            (1.0 + (-6.0 + (4.0 - 8.0/(15.0 * t)) / t) / t);
}

double RhoGaussLowRank::g6_(const double t) const {
    if (t < ZERO_LIMIT_gx_) return 0.0;
    return -exp(-1.0 / t) / pow(t, 4.5) *
            (1.0 + (-2.0 + (4.0 / 5.0 - 8.0/(105.0 * t)) / t) / t);
}

double RhoGaussLowRank::f0_(const double b, const double x) const {
    const double d_b2 = 1.0 / (b * b);
    return  d_sqrt_pi_ * d_b2 * g0_(d_b2 * x);
}

double RhoGaussLowRank::f1_(const double b, const double x) const {
    const double d_b2 = 1.0/(b*b);
    return  0.5 * d_sqrt_pi_ * d_b2 / b * g1_(d_b2 * x);
}

double RhoGaussLowRank::f2_(const double b, const double x) const {
    const double d_b2 = 1.0/(b*b);
    return  1.5 * d_sqrt_pi_ * d_b2 * d_b2 * g2_(d_b2 * x);
}

double RhoGaussLowRank::f3_(const double b, const double x) const {
    const double d_b2 = 1.0/(b*b);
    return  0.75 * d_sqrt_pi_ * d_b2 * d_b2 / b * g3_(d_b2 * x);
}

double RhoGaussLowRank::f4_(const double b, const double x) const {
    const double d_b2 = 1.0/(b*b);
    return  3.75 * d_sqrt_pi_ * d_b2 * d_b2 * d_b2 * g4_(d_b2 * x);
}

double RhoGaussLowRank::f5_(const double b, const double x) const {
    const double d_b2 = 1.0/(b*b);
    return  1.875 * d_sqrt_pi_ * d_b2 * d_b2 * d_b2 / b * g5_(d_b2 * x);
}

double RhoGaussLowRank::f6_(const double b, const double x) const {
    const double d_b2 = 1.0/(b*b);
    const double d_b4 = d_b2 * d_b2;
    return  13.125 * d_sqrt_pi_ * d_b4 * d_b4 * g6_(d_b2 * x);
}

///////////////////////////////////////////////////////////////////////////////

// End of the file

