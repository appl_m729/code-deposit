#ifndef _READ_ATOM_H_
#define _READ_ATOM_H_

// input service routines

// these are auxiliary procedures needed 
// to read input parameters from the input file

////////////////////////////////////////////////////////////////////////////

int ReadInt( const char* const file_name, const char* const parameter_name,
             int position, int &input_value);

int ReadReal(const char* const file_name, const char* const parameter_name, 
             int position, double &input_value);

int ReadStr( const char* const file_name, const char* const parameter_name, 
             int position, char* input_value);

int ReadIntStr(const char* const file_name, const char* const parameter_name, 
               int position, int &input_value);

const char *Error_read(int nErr);

////////////////////////////////////////////////////////////////////////////

#endif // _READ_ATOM_H_

// end of file
