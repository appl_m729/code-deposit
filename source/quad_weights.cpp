#include <cstdlib>
#include <iostream>
#include "quad_weights.h"

using std::cout;
using std::endl;

///////////////////////////////////////////////////////////////////////////////

QuadWeights::QuadWeights():
N_flag_ok_(false),
N_(0),
w_(0) {

}

// virtual
QuadWeights::~QuadWeights() {
    delete[] w_;   w_ = 0;
}

void QuadWeights::set_size(const int N) {

    if (N < 2) {
        cout << " Error in QuadWeights::set_size;  too small N = " << N << endl;
        exit(0);
    }

    N_ = N;
    delete[] w_; w_ = 0;
    w_ = new double[N_ + 1];

    N_flag_ok_ = true;
}

// virtual
void QuadWeights::setup_weights() {

    if (!N_flag_ok_) {
        cout << "Error in QuadWeights setup..." << endl;
        exit(0);
    }

    for (int i = 0; i <= N_; ++i) {
        w_[i] = 1.0;
    }
}

int QuadWeights::N() const {
    return N_;
}

double QuadWeights::w(const int i) const {
    return w_[i];
}

///////////////////////////////////////////////////////////////////////////////

TrapezoidWeights::TrapezoidWeights():
QuadWeights() {
}

TrapezoidWeights::~TrapezoidWeights() {
}

// virtual
void TrapezoidWeights::setup_weights() {

    if (!N_flag_ok_) {
        cout << "Error in TrapezoidWeights setup..." << endl;
        exit(0);
    }

    for (int i = 1; i < N_; ++i) {
        w_[i] = 1.0;
    }
    w_[0] = w_[N_] = 0.5;
}

///////////////////////////////////////////////////////////////////////////////

SimpsonWeights::SimpsonWeights():
QuadWeights() {
}

SimpsonWeights::~SimpsonWeights() {
}

// virtual
void SimpsonWeights::setup_weights() {

    if (!N_flag_ok_) {
        cout << "Error in SimpsonWeights setup..." << endl;
        exit(0);
    }
    // N is ODD - wrong for simpson rule
    if (N_ % 2) {
        cout << "Error in SimpsonWeights N = " << N_ << endl;
        cout << "It must be EVEN" << endl;
        exit(0);
    }

    const double one_over_3 = static_cast<double>(1.0) / 3.0;
    const int N_over_2 = N_ / 2;

    for (int i = 1; i < N_over_2; ++i) {
        w_[2 * i - 1] = 4.0 * one_over_3;
        w_[2 * i]     = 2.0 * one_over_3;
    }
    w_[N_ - 1] = 4.0 * one_over_3;

    w_[0] = w_[N_] = one_over_3;
}

///////////////////////////////////////////////////////////////////////////////

// End of the file
