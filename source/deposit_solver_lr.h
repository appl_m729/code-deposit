#ifndef _LOW_RANK_DEPOSIT_SOLVER_H_
#define _LOW_RANK_DEPOSIT_SOLVER_H_

///////////////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include "interpolation.h"

class DepositParser;
class IntegralTb_TensorSum;
class Probability_mfold;

using namespace alglib;


/*! Main solver of the Deposit code.                                  *
 *  Computation of the deposited energy is based                      *
 *  on the low rank approximation technique.                          *
 *                                                                    *
 *  Reads input. Calcilates T(b), Pm(b) and Sigma (total and m-fold). *
 *  Writes out the results in proper format.                          */

class DepositSolverLR {
public:
    DepositSolverLR();
    ~DepositSolverLR();

    // Does all the reading, calculations and output.
    void solve(const char inp_file[], const char out_file[]);

    // writes deposited energy T(b) (total and shells)
    void write(const char out_file[]) const;

private:
    DepositParser        * const d_parser_;       // parser
    IntegralTb_TensorSum * const integral_lr_Tb_; // Tb integral (low rank sum)
    Probability_mfold    * const Pm_fold_;        // Probabilities calculations

    const int MAX_ITER_;    // bisection max interation number
    int      Nb_;           // number of all b-points
    int      l_tot_;        // l-index of b and T(b) such that T(b[lb_max]) - I1 = 0
    double   b_tot_;        // bmax on the b-mesh (T(b_max) - I1 = 0)
    double * Tb_, * bl_;    // array of Tb and array of bl; Tb(bl)
    double * Tbk_;          // Tb array for a shell
    double   sig_tot_;      // total cross section sig = pi b_tot ^ 2

    time_t   tt_start_;

    // Deallocates bl[], Tbl[] - total, Tbkl[] - for fixed shell
    void   free_Tb_arrays_();
    // Allocates bl[], Tbl[], Tbkl[]
    void   allocate_Tb_arrays_();

    // Main calculation of Tb(b).
    // Tb(b) is cross-decomposed here for every shell.
    void   calc_energy_Tb_();

    // Calculates product of cross matrices and copy it to Tb array
    void   calc_Tb_data_to_Tb_array_();  // for currently fixed shell

    // Total time of the Deposit
    void   measure_time_();

    // Prints (b, Tb) in a formated representation
    void   b_Tb_out_(const double b, const double Tb) const; // formatted print_out

    // Main routine which finds b_max (and Sigma_tot = PI * b_max^2)
    void   calc_Sigma_tot_();

    // First stage bisection. The points already calculated are used.
    void   Tb_bisection_on_mesh_(int & lb_left, int & lb_right);

    // 10 point-sample is used for cubic natural spline [left - 4, right + 4]
    void   create_spline_interpolant_(const int lb_left, const int lb_right,
                                      spline1dinterpolant & s1i);
    // extrapolates Tb points by spline and finds
    // b_max from b_tot_extrapolate_(...)
    double Tb_spline_bisection_(const int lb_left, const int lb_right);

    // Finish b-mesh constructing. See inplementation in .cpp
    void finish_b_Tb_right_bound_(const int lb_left, spline1dinterpolant & s1i,
                                  const double max_b_extr);

    // Returns b_max extrapolated from parabola
    // (3 parabola points are found from spline interpolation)
    double b_tot_extrapolate_(const double x_left, const double x_right,
                              spline1dinterpolant & s1i) const;
    // parabola minimum
    double parabola_extrapolate_(const double x1, const double y1,
                                 const double x2, const double y2,
                                 const double x3, const double y3) const;
    // Calculates Sigma m-fold by using Pm(b)
    void   calc_Sigma_m_fold_();

    // Integral Pm(b) * b
    double integrate_sigma_mfold_(const int m) const;

    // Print percent of totally calculated Tb integrals on mesh
    // for a given shell
    void  print_percent_(const int value) const;

    // Write out the deposited energy
    void  write_Tb_(const char out_file[]) const;

    // Write out the probabilities Pm
    void  write_Pm_(const char out_file[]) const;

    // Check Sum_m(Pm) in every b_l
    void  check_Pm_();
};

///////////////////////////////////////////////////////////////////////////////

#endif   // _LOW_RANK_DEPOSIT_SOLVER_H_

// End of the file

