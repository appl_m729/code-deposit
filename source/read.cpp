#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include "read.h"

////////////////////////////////////////////////////////////////////////////

using namespace::std;


const int nStrLen = 256;
const char  Delim[] = " :=\"'\t,;\n";
const char* EndComm[]  = { "//", "!", NULL}; 
const char* ForwComm[] = { "/*", "<", NULL};
const char* BackComm[] = { "*/", ">", NULL};

int GetLine(istream &In, char* const Buff, int BuffLen) {

  int i;
  char *Buffer = NULL, *NextBuffer = NULL, *Start, *Finish;
  if( !In.good()) return 8;
  Buffer = new char[BuffLen];
  if( Buffer == NULL) return 7;
  NextBuffer = new char[BuffLen];
  if( NextBuffer == NULL){
    delete Buffer;
    return 7;
  }
  In.getline(Buffer, BuffLen, '\n');
  for(i = 0; ForwComm[i] != NULL; i++){
    strcpy(NextBuffer, Buffer);
    while((Start = strstr(Buffer, ForwComm[i])) != NULL){
      Finish = strstr(Start, BackComm[i]);
      while((Finish == NULL) && !In.eof()){
        In.getline(NextBuffer,BuffLen,'\n');
        Finish = strstr(NextBuffer, BackComm[i]);
      }
      if(In.eof()){
        delete[] Buffer;
        delete[] NextBuffer;
        return 1;
      }
      if(Start - Buffer + strlen(Finish+strlen(BackComm[i])) > BuffLen - 1){
        delete[] Buffer;
        delete[] NextBuffer;
        return 2;
      }
      strcpy(Start,Finish + strlen(BackComm[i]));
    }
  }
  for(i = 0; EndComm[i] != NULL; i++){
    Finish = strstr(Buffer,EndComm[i]);
    if(Finish != NULL) *Finish = 0;
  } 
  strcpy(Buff, Buffer);
  delete[] Buffer;
  delete[] NextBuffer;
  return 0;      
}

int ReadInt( const char* const File, const char* const ParamName,
             int Position, int &Result){
// Input ::  File - name of file to read, ParamName - name of parameter
//           to read from File, Position - count the number after ParamName
//           to read from File.
// OutPut :: Result - readen value
// Return :: 0 - OK,
//           1 - number in Position can't be found, 
//           2 - number is Real, but readed as Int ( or unexpected '.').
//           3 - field Position is text, not number 
//           4 - pair closing comment symbol expected 
//           5 - line is too long
//           6 - Parameter not found
//           7 - not enough memory
//           8 - bad file name
  char Buffer[nStrLen], *Start, *Finish;
  int i = 0;
  ifstream In(File);
  if( !In.good())
    return 8;
  do{
    switch(GetLine(In, Buffer, nStrLen)){
    case 1:  
      return 4;
    case 2:
      return 5; 
    case 7:
      return 7;
    case 8:
      return 8;
	}  
    Start = Finish = Buffer;
    do{
      Start = strstr(Start, ParamName);
      if (Start != NULL){
        if (Start != Buffer){
          if ((strchr(Delim, Start[strlen(ParamName)]) != NULL) &&
              (strchr(Delim, *(Start-1)) != NULL)) Finish = NULL;
		}else{
          if(strchr(Delim, Start[strlen(ParamName)]) != NULL)
            Finish = NULL;
		}
        Start += strlen(ParamName);
	  }else Finish = NULL;
	}while(Finish != NULL);
    if(Start != NULL){
      do{
        Start += strspn(Start, Delim);
        while((strlen(Start) != 0) && (i <= Position)){
          Finish = Start + strcspn(Start, Delim);
          if (i == Position){
            Result = strtol(Start, &Finish, 10);
            if (Finish[0] == '.')
              return 2;
            if (Start == Finish)
              return 3;
            return 0;
          }else{
            i++;
            Start = Finish;
          }
          Start += strspn( Start, Delim);
        }
        switch(GetLine(In, Buffer, nStrLen)){
        case 1:
          return 4;
        case 2:
          return 5;
        case 7:
          return 7;
        case 8:
          return 8;
        }
        Start = Buffer;
      }while(!In.eof() && (Start != NULL));
      if(In.eof()) return 1;
    }
  }while(!In.eof());
  return 6;
}

int ReadReal(const char* const File, const char* const ParamName,
             int Position, double &Result){
// Input ::  File - name of file to read,  ParamName - name of parameter
//           to read from File, Position - count the number after ParamName
//           to read from File.
// OutPut :: Result - readen value
// Return :: 0 - OK,
//           1 - number in Position can't be found, 
//           3 - field Position is text, not number 
//           4 - pair closing comment symbol expected 
//           5 - line is too long
//           6 - Parameter not found
//           7 - not enough memory
//           8 - bad file name
  char Buffer[nStrLen], *Start, *Finish;
  int i = 0;
  ifstream In( File);
  if( !In.good()) return 8;
  do{
    switch(GetLine(In, Buffer, nStrLen)){
    case 1:
      return 4;
    case 2:
      return 5; 
    case 7:
      return 7;
    case 8:
      return 8;
    }  
    Start = Finish = Buffer;
    do{
      Start = strstr(Start, ParamName);
      if (Start != NULL){
        if (Start != Buffer){
          if ((strchr(Delim, Start[strlen(ParamName)]) != NULL) &&
              (strchr( Delim, *(Start-1)) != NULL)) Finish = NULL;
		}else{
          if (strchr(Delim, Start[ strlen( ParamName)]) != NULL)
            Finish = NULL;
        }
        Start += strlen( ParamName);
      }else Finish = NULL;
    }while( Finish != NULL);
    if (Start != NULL){
      do{
        Start += strspn(Start, Delim);
        while ((strlen(Start) != 0) && (i <= Position)){
          Finish = Start + strcspn(Start, Delim);
          if( i == Position){
            Result = strtod(Start, &Finish);
            if (Start == Finish) return 3;
            return 0;
          }
          else{
            i++;
            Start = Finish;
          }
          Start += strspn(Start, Delim);
        }
        switch(GetLine(In, Buffer, nStrLen)){
        case 1:
          return 4;
        case 2:
          return 5;
        case 7:
          return 7;
        case 8:
          return 8;
		}
        Start = Buffer;
      }while( !In.eof() && ( Start != NULL));
      if( In.eof()) return 1;
    }
  }while( !In.eof());
  return 6;
}

int ReadStr(const char* const File, const char* const ParamName,
            int Position, char* Result){
// Input ::  File - name of file to read,  ParamName - name of parameter
//           to read from File, Position - count the number after ParamName
//           to read from File.
// OutPut :: Result - readen value
// Return :: 0 - OK,
//           1 - number in Position can't be found, 
//           4 - pair closing comment symbol expected 
//           5 - line is too long
//           6 - Parameter not found
//           7 - not enough memory
//           8 - bad file name
  char Buffer[nStrLen], *Start, *Finish;
  int i = 0;
  ifstream In( File);
  if (!In.good()) return 8;
  do{
    switch(GetLine(In, Buffer, nStrLen)){
    case 1:
      return 4;
    case 2:
      return 5; 
    case 7:
      return 7;
    case 8:
      return 8;
    }  
    Start = Finish = Buffer;
    do{
      Start = strstr(Start, ParamName);
      if (Start != NULL){
        if (Start != Buffer){
          if ((strchr(Delim, Start[ strlen( ParamName)]) != NULL) &&
              (strchr(Delim, *(Start-1)) != NULL)) Finish = NULL;
		}else{
          if( strchr( Delim, Start[ strlen( ParamName)]) != NULL)
            Finish = NULL;
		}
        Start += strlen(ParamName);
      }
      else Finish = NULL;
    }while (Finish != NULL);
    if (Start != NULL){
      do{
        Start += strspn(Start, Delim);
        while ((strlen(Start) != 0) && (i <= Position)){
          Finish = Start + strcspn(Start, Delim);
          if (i == Position){
            strncpy(Result, Start, Finish - Start);
            Result[Finish - Start] = 0;
            return 0;
          }
          else{
            i++;
            Start = Finish;
          }
          Start += strspn(Start, Delim);
        }
        switch (GetLine(In, Buffer, nStrLen)){
        case 1:
          return 4;
        case 2:
          return 5;
        case 7:
          return 7;
        case 8:
          return 8;
        }
        Start = Buffer;
      }while(!In.eof() && (Start != NULL));
      if( In.eof())
        return 1;
    }
  }while (!In.eof());
  return 6;
}

int ReadIntStr(const char* const file_name, const char* const parameter_name, 
               int position, int &input_value){
 char ch[10];
 int key=ReadStr(file_name,parameter_name,position,ch);
 if(key!=0){cout<<"Error reading in IntStr"<<endl;exit(1);}
     cout<<ch;
     if(strchr(ch,'*')){
       ReadInt(file_name,parameter_name,position,input_value);
       return 1;
     }else{
       ReadInt(file_name,parameter_name,position,input_value);
       return 2;
     }
// cout<<endl;
}

const char *err_mesage[] = {
"          0 - it is OK",
"          1 - number in Position can't be found", 
"          2 - number is Real, but readed as Int ( or unexpected '.')",
"          3 - field Position is text, not number", 
"          4 - pair closing comment symbol expected", 
"          5 - line is too long",
"          6 - parameter not found",
"          7 - not enough memory",
"          8 - bad file name",
"              wrong error number"};

const char *Error_read(int nErr){
     if((nErr>8)||(nErr<0)) nErr = 9;
     return err_mesage[nErr]; 
}

char i2char(int i){
 if(i>=0 && i<=9){
   char c='0'+i;
   return c;
 }else {cout<<"Error! in i2char(int i)"<<endl;exit(0);}
}

void i_to_str(int i,char str[]){
 int j=0, ii[20], k, is =1;
 if (i<0){
   i =-i;
   is=-1;
 }
 do{
   ii[j++]=i%10;
   i/=10;
 }while(i);
 k=0;
 if (is==-1) str[k++] ='-';
 do{
   str[k++] =i2char(ii[--j]); 
 }while (j>=1);
 str[k]=0;
}

////////////////////////////////////////////////////////////////////////////

// end of file


