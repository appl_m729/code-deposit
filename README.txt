
This folder is a Root folder for the DEPOSIT 2014 code.

The DEPOSIT code is intended to calculate total and m- fold 
electron-loss cross-sections (m is the number of ionized electrons) 
and the energy T(b) deposited to the projectile (positive or negative ion) 
during a collision with a neutral atom at low and intermediate collision 
energies as a function of the impact parameter b. The deposited energy is
calculated as a 3D integral over the projectile coordinate space 
in the classical energy-deposition model. Examples of calculated 
deposited energies, ionization probabilities and electron-loss 
cross-sections are given as well as the description of the input and output data.

It contains the following nested folders
and files:

source2014v2
(The source code for single-core DEPOSIT 2014 version)

tests
(Two examples. The DEPOSIT program input and output files)

users_guide
Documentation how to make program, run it and 
analyze output data. The description of the 
code structure is presented as well.

The DEPOSIT code should be referred to as follows:
M.S. Litsarev, I.V. Oseledets. Comput. Phys. Comm. 185, 2801 (2014).
M.S. Litsarev. Comput. Phys. Comm. 184, 432 (2013).
V.P. Shevelko, D. Kato, M.S. Litsarev, H. Tawara. J. Phys. B: At. Mol. Opt. Phys. 43, 215202 (2010).
V.P. Shevelko, M.S. Litsarev, H. Tawara. J. Phys. B: At. Mol. Opt. Phys. 41, 115204 (2008).


